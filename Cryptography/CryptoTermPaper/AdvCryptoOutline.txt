OVERVIEW

Outline
    outline

What is LAC?
    What are it’s crypto systems
    What crypto systems will I focus on
    Authors

Design
    Why it’s considered good

Lattice stuff / Core concepts
    SVP
    CVP
    LWE
    LWE-Rings

Public Key system
    How it’s implemented
    Enc/Dec

Passively Secure Key Exchange protocol
    How it works

Performance Analysis

Future work if possible
    Other 2 schemes within LAC

REFERENCES
    LAC
    PEI
    Other documents for lattices




CHECK OUT COMMENTS OF LAC ON NIST
SHOW PSEUDOCODE
	ANOTHER SLIDE EXPLAINING PSEUDO-CODE?
CITE AUTHORS - What else have they done?
WHO ELSE STUDIED THIS CRYPTOSYSTEM?
PERFORMANCE TESTS/DATA
KEEP IT SHORT
