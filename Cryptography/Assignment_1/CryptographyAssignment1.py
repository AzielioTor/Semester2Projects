"""
Aziel Shaw - 309000893
Advanced Cryptography Assignment 1
25/01/2018 # page 235
"""
import math
import operator


"""
Takes the modular inverse of (y1 ^ a) ^ -1 mod p
"""
def modInverse(y1, a, p):
    answer1 = pow(y1, a)
    return sm(answer1, p-2, p)


"""
Square and multiply algorithm used to help calculate the primality within
    miller_rabin_main, will return x^e mod m
INPUT: x, number to be multiplied
       e, the exponent to multiply x to
       m, number to modulo x^e
OUTPUT: x^e mod m
"""
def sm(x, e, m):
    # Convert e into binary and remove leading 2 digits "0b"
    e = int(e)
    e = bin(e)
    e = e[2:]
    # Perform Square-and-Multiply algorithm
    result = 1
    eLength = len(e)
    for i in range(0, eLength):
        result = pow(result, 2, m)
        if e[i] == '1':
            result = (result * x) % m
    return result


"""
Elgamal Encryption scheme.
Takes in a prime number, a number alpha, an number "a", a message to send, and a key
"""
def elgamalEnc(prime, alpha, a, message, key):
    beta = pow(alpha, a, prime)
    y1 = pow(alpha, key, prime)
    y2 = (message * pow(beta, key, prime)) % prime
    return y1, y2


"""
Elgamal Decryption scheme.
Takes in a prime number, a number alpha, an number "a", a message to send, and a key
"""
def elgamalDec(y1, y2, a, p):
    # y2(y1^a)^-1  mod p
    inverse = modInverse(y1, a, p)
    return ((y2 * inverse) % p)


"""
Runs through the encryption and decryption on a set of parameters in order to test my code.   
"""
def completeelgamal(prime, alpha, a, key, message):
    print("Prime number is : " + str(prime))
    print("alpha number is : " + str(alpha))
    print(" \"a\"  number is : " + str(a))
    print("  key number is : " + str(key))
    print("mesge number is : " + str(message))
    print("\nEncrypting Elgamal...")
    y1, y2 = elgamalEnc(prime, alpha, a, message, key)
    print("  y1  number is : " + str(y1))
    print("  y2  number is : " + str(y2))
    print("\nDecrypting Elgamal...")
    result = elgamalDec(y1, y2, a, prime)
    print("Result is :  " + str(result))
    print("Message is : " + str(message))
    print("\n\n\n\n")


"""
Decodes single number into 3 letters
"""
def enigmaEncoder(number):
    lett1 = number // pow(26, 2)
    lett2 = (number - (lett1 * pow(26, 2))) // 26
    lett3 = (number - (lett1 * pow(26, 2)) - (lett2 * 26))
    return str(chr(lett1 + 65) + chr(lett2 + 65) + chr(lett3 + 65))


"""
Guess the key for the cipher text using brute force
"""
def guessKeyBruteForce(prime, alpha, a, y1, y2):
    message = (elgamalDec(y1, y2, a, prime))
    key = "ERROR"
    for i in range(0, prime - 1):
        x1, x2 = elgamalEnc(prime, alpha, a, message, i)
        if x1 == y1 and x2 == y2:
            print("Found Key for :  " + str(message) + " | key :   " + str(i))
            return i
    print("No Key :  " + str(message))
    return key


"""
Guess the key for the cipher text using shanks algorithm
"""
def guessKeyShanksAlgorithm(prime, alpha, a, y1, y2):
    return shanks(prime, prime - 1, alpha, y1)


"""
Shanks Algorithm.
From the textbook.
"""
def shanks(G, n, alpha, beta):
    # Step 1
    m = math.ceil(math.sqrt(n))
    # Step 2
    computed1 = []
    for j in range(0, int(m)):
        computed1.append((j, pow(alpha, (m * j), G)))
    # Step 3
    l1 = computed1
    l1.sort(key=operator.itemgetter(1))
    # Step 4
    computed2 = []
    for i in range(0, int(m)):
        computed2.append((i, ((beta * (pow(pow(alpha, i), G-2))) % G)))
    # Step 5
    l2 = computed2
    l2.sort(key=operator.itemgetter(1))
    # Step 6
    for tup1 in range(0, len(l1)):
        tup1v = l1[tup1]
        for tup2 in range(0, len(l2)):
            tup2v = l2[tup2]
            # Step 7
            if tup1v[1] == tup2v[1]:
                return ((m * tup1v[0]) + tup2v[0]) % n
    # Step 8 If nothing is found
    return "ERROR"




print(shanks(809, 808, 3, 525))



if __name__ == "__main__":
    prime = 31847
    alpha = 5
    a = 7899
    # key = 853
    # Decipher ciphertext
    readFile = open("ElGamalcipher.txt", "r")
    result = ""
    for line in readFile:
        line = line[:-1]
        line = line.split()
        y1 = int(line[0])
        y2 = int(line[1])
        result += (enigmaEncoder(elgamalDec(y1, y2, a, prime)))
    writeFile = open("output.txt", "w")
    writeFile.write(result)


    print("\n##########BRUTE##########\n\n")


    # Brute force keys
    readFile = open("ElGamalcipher.txt", "r")
    result = ""
    for line in readFile:
        line = line[:-1]
        line = line.split()
        y1 = int(line[0])
        y2 = int(line[1])
        result += str(guessKeyBruteForce(prime, alpha, a, y1, y2)) + "\n"
    writeFile = open("BruteForceKeys.txt", "w")
    writeFile.write(result)


    print("\n\n##########SHANK##########\n\n")


    # Shanks Algorithm for getting key.
    readFile = open("ElGamalcipher.txt", "r")
    result = ""
    for line in readFile:
        line = line[:-1]
        line = line.split()
        y1 = int(line[0])
        y2 = int(line[1])
        shanksvalue = guessKeyShanksAlgorithm(prime, alpha, a, y1, y2)
        print("Shanks :  " + str(shanksvalue))
        result += str(shanksvalue) + "\n"
    writeFile = open("ShanksAlgorithmKeys.txt", "w")
    writeFile.write(result)

    print("\n\n##########COMPARE##########\n\n")

    # Compare Shanks to Brute Force
    shankfile = open("ShanksAlgorithmKeys.txt", "r")
    brutefile = open("BruteForceKeys.txt", "r")
    for index in range(0, 102):
        shank = shankfile.readline()
        brute = brutefile.readline()
        if shank == brute:
            print("KEY SAME")
        else:
            print("KEY DIFFERENT")
    print("ENDING PROGRAM")

