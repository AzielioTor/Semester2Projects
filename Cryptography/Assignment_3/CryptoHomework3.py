"""
Advanced Cryptography Homework 3
Aziel Shaw - 309000893
08/02/2018
"""
import datetime as dt
import math
import operator
import random


"""Pollard_Rho algorithm"""
def pollard_rho(n, x_1):
    x = x_1
    x_prime = function_f(x, n)
    p = math.gcd((x - x_prime), n)
    counter = 1
    while p == 1:
        counter = counter + 1
        # In the ith iteration, x = x_i and x_prime = x_2i
        x = function_f(x, n)
        x_prime = function_f(x_prime, n)
        x_prime = function_f(x_prime, n)
        p = math.gcd((x - x_prime), n)
    print("Counter for Pollard Rho :  " + str(counter))
    if p == n:
        return "Failure"
    else:
        return p


"""Function f for Pollard Rho"""
def function_f(x):
    return pow(x, 2) + 1


"""Function f for Pollard Rho mod n"""
def function_f(x, n):
    return (pow(x, 2) + 1) % n


"""Pollard_Rho algorithm Brents accumulator Method"""
def pollard_rho_brents(n, x_1, k):
    x = x_1
    x_prime = function_f(x, n)
    p = math.gcd((x - x_prime), n)
    counter = 1
    z = 1
    while True:
        # In the ith iteration, x = x_i and x_prime = x_2i
        x = function_f(x, n)
        x_prime = function_f(x_prime, n)
        x_prime = function_f(x_prime, n)
        counter = counter + 1
        z = z * (x - x_prime) % n
        if not (counter % k):
            p = math.gcd(z, n)
            if 1 < p < n:
                break
            else:
                z = 1
    # Exited while
    print("Counter for Pollard Rho DL :  " + str(counter))
    if p == n:
        return "Failure"
    else:
        return p


"""Shanks Algorithm coded from the textbook"""
def shanks(G, n, alpha, beta):
    # Step 1
    m = math.ceil(math.sqrt(n))
    # Step 2
    computed1 = []
    for j in range(0, int(m)):
        computed1.append((j, pow(alpha, (m * j), G)))
    # Step 3
    l1 = computed1
    l1.sort(key=operator.itemgetter(1))
    # Step 4
    computed2 = []
    for i in range(0, int(m)):
        computed2.append((i, ((beta * (pow(pow(alpha, i), G-2))) % G)))
    # Step 5
    l2 = computed2
    l2.sort(key=operator.itemgetter(1))
    # Step 6
    print("Searching for match")
    for tup1 in range(0, len(l1)):
        tup1v = l1[tup1]
        for tup2 in range(0, len(l2)):
            tup2v = l2[tup2]
            # Step 7
            if tup1v[1] == tup2v[1]:
                return ((m * tup1v[0]) + tup2v[0]) % n
    # Step 8 If nothing is found
    return "ERROR"


"""Pollard Rho Discrete Log"""
def pollard_rho_discrete_log(G, n, alpha, beta):
    # First line in main from textbook
    s1 = []
    s2 = []
    s3 = []
    for i in range(0, G+1):
        j = i % 3
        if j == 1:
            s1.append(i)
        elif j == 0:
            s2.append(i)
        else:
            s3.append(i)
    # Second line in main from textbook
    x, a, b = f(1, 0, 0, s1, s2, n, alpha, beta, G)
    # Third line in main from textbook
    xp, ap, bp = f(x, a, b, s1, s2, n, alpha, beta, G)
    # While loop in main in textbook
    while x != xp:
        # First do
        x, a, b = f(x, a, b, s1, s2, n, alpha, beta, G)
        # Second do
        xp, ap, bp = f(xp, ap, bp, s1, s2, n, alpha, beta, G)
        # Third do
        xp, ap, bp = f(xp, ap, bp, s1, s2, n, alpha, beta, G)
    # If statement in main in textbook
    if math.gcd((bp - b), n) != 1:
        d = math.gcd((bp - b), n)
        s = n // 2
        c_1 = d % 2
        c_2 = ((a - ap) * pow((bp - b), s - 2, s)) % s
        # CRT
        M = n
        m_1 = n // 2
        m_2 = 2
        M_1 = M // m_1
        M_2 = M // m_2
        Mi_1 = pow(M_1, m_1 - 2, m_1)
        # Mi_1 = extendedEuclidsAlgo(min(M_1, m_1), max(M_1, m_1))[1]
        Mi_2 = extendedEuclidsAlgo(M_2, m_2)[1]
        b_1 = (M_1 * Mi_1) % M
        b_2 = (M_2 * Mi_2) % M
        return (c_1 * b_1) + (c_2 * b_2) % M
    # Return  logic in main
    else:
        return ((a - ap) * pow((bp - b), n - 2, n)) % n


"""Helper function for Pollard Rho Discrete Logarithm"""
def f(x, a, b, s1, s2, n, alpha, beta, G):
    if x in s1:
        return ((beta * x) % G), a, (b + 1) % n
    elif x in s2:
        return pow(x, 2, G), (2 * a) % n, (2 * b) % n
    else:
        return ((alpha * x) % G), (a + 1) % n, b


""""a mod b, returns inverse as Tuple"""
def extendedEuclidsAlgo(a, b):
    if b == 0:
        return (a, 1, 0)
    else:
        q, r = a // b, a % b
        u, s, t = extendedEuclidsAlgo(b, r)
        return (u, t, s - q * t)


"""
Implementation of Miller Rabin from the understanding cryptography textbook
INPUT: prime number p, and security parameter s
OUTPUT: 1 if probably prime, 0 if not prime
"""
def miller_rabin(p, s):
    # generate "r"
    r = p - 1
    u = 0
    while r % 2 == 0:
        u = u + 1
        r = (r // 2)

    for i in range(1, s - 1):
        a = random.randint(2, p - 2)
        z = pow(a, r, p)
        if z != 1 and z != p - 1:
            for j in range(1, u):
                z = pow(z, 2, p)
                if z == 1:
                    return 0
            if z != p - 1:
                return 0
    return 1


"""
Generates random RSA-safe prime number of b bits length.
INPUT: b = the length (in bits) of the prime number
       s = the security number to test the primality. The bigger the number the greater chance of RSA-safe prime.
OUTPUT: A likely prime number of b bits.
"""
def RSA_safe_prime_generator(b, s):
    prime_found = False
    prime = -1
    while not prime_found:
        while not prime_found:
            # Turn randomly generated number (in bits) into integer
            potential_prime = random.getrandbits(b - 1)
            # Primality Test the random number
            is_prime = miller_rabin(potential_prime, s)
            # If number is prime, break the first while loop
            if is_prime == 1:
                prime = potential_prime
                break
        # Test if the (primenumber * 2) + 1 is still prime.
        prime = (prime * 2) + 1
        if miller_rabin(prime, s) == 1 and len(bin(prime)) == b:
            prime_found = True

    return prime


"""
Generates random prime number of b bits length.
INPUT: b = the length (in bits) of the prime number
       s = the security number to test the primality. The bigger the number the greater chance of prime.
OUTPUT: A likely prime number of b bits.
"""
def prime_generator(b, s):
    prime_found = False
    prime = -1
    while not prime_found:
        # Turn randomly generated number (in bits) into integer
        potential_prime = random.getrandbits(b - 1)
        # Primality Test the random number
        is_prime = miller_rabin(potential_prime, s)
        # If number is prime, break the first while loop
        if is_prime == 1:
            prime = potential_prime
            prime_found = True
    return prime

"""
Square and multiply algorithm used to help calculate the primality within
    miller_rabin_main, will return x^e mod m
INPUT: x, number to be multiplied
       e, the exponent to multiply x to
       m, number to modulo x^e
OUTPUT: x^e mod m
"""
def sm(x, e, m):
    # Convert e into binary and remove leading 2 digits "0b"
    e = int(e)
    e = bin(e)
    e = e[2:]

    # Perform Square-and-Multiply algorithm
    result = 1
    eLength = len(e)
    for i in range(0, eLength):
        result = pow(result, 2, m)
        if e[i] == '1':
            result = (result * x) % m
    return result

"""This is the main method for the program"""
def main():
    # test_algorithms()
    # homework_part_1()
    # homework_part_2()
    homework_part_3()
    return


"""This method tests all the algorithms created in this assignment to test if the numbers work like they should be"""
def test_algorithms():
    print("Pollard Rho")
    n = 15770708441
    prime = 809
    not_prime = 999
    new_num = 10403
    x_1 = 10
    print(pollard_rho(n, x_1))
    print(pollard_rho(prime, x_1))
    print(pollard_rho(not_prime, x_1))
    print(pollard_rho(new_num, x_1))

    print("\nExtended Euclids Algorithm")
    print(extendedEuclidsAlgo(15, 26))

    print("\nPollard Rho DL")
    G = 809
    n = 101
    alpha = 89
    beta = 618
    print(pollard_rho_discrete_log(G, n, alpha, beta))
    print("Answer should be 49")

    print("\nPollard Rho w/ Brents accumulator")
    n = 15770708441
    prime = 809
    not_prime = 999
    new_num = 10403
    x_1 = 10
    k = 10
    print(pollard_rho_brents(n, x_1, k))
    #print(pollard_rho_brents(prime, x_1, k))
    #print(pollard_rho_brents(not_prime, x_1, k))
    print(pollard_rho_brents(new_num, x_1, k))

    print("Miller Rabin")
    prime = 101
    non_prime = 100
    n_1 = 15486517
    n_2 = 15486518
    n_3 = 12345663
    s = 7
    print("Should be 1, then 0, 1, 0, 0")
    print(miller_rabin(prime, s))
    print(miller_rabin(non_prime, s))
    print(miller_rabin(n_1, s))
    print(miller_rabin(n_2, s))
    print(miller_rabin(n_3, s))

    print("\n Prime Generation")
    s = 7
    #print(prime_generator(10, 5))
    p_1 = prime_generator(100, s)
    p_2 = prime_generator(50, s)
    print("Number :  " + str(p_1) + " Is a prime number. With :  " + str(len(bin(p_1))) + "Bits.")
    print("Number :  " + str(p_2) + " Is a prime number. With :  " + str(len(bin(p_2))) + "Bits.")
    return


"""This method runs all the code needed for the first part of the homework"""
def homework_part_1():
    print("HOMEWORK QUESTION 1")
    print("RSA-safe-primes")
    s = 9
    print("Security Value s = " + str(s) + "\n")

    for time in range(10, 80, 10):
        start_time = dt.datetime.now()
        print(str(time) + " RSA safe b bit prime number : ")
        print(RSA_safe_prime_generator(time, s))
        print("Time Taken  : " + str(dt.datetime.now() - start_time) + "\n")

    start_120 = dt.datetime.now()
    print("\n120 RSA safe b bit prime number : ")
    print(RSA_safe_prime_generator(120, s))
    print("Time Taken  : " + str(dt.datetime.now() - start_120) + "\n")

    start_200 = dt.datetime.now()
    print("\n200 RSA safe b bit prime number : ")
    print(RSA_safe_prime_generator(200, s))
    print("Time Taken  : " + str(dt.datetime.now() - start_200) + "\n")

    start_500 = dt.datetime.now()
    print("\n500 RSA safe b bit prime number : ")
    print(RSA_safe_prime_generator(500, s))
    print("Time Taken  : " + str(dt.datetime.now() - start_500) + "\n")

    #################################################

    print("\n#################################################"
          "\nnpn-RSA-safe-primes")
    s = 9
    print("Security Value s = " + str(s))

    for time in range(10, 80, 10):
        start_time = dt.datetime.now()
        print(str(time) + " non RSA safe b bit prime number : ")
        print(prime_generator(time, s))
        print("Time Taken  : " + str(dt.datetime.now() - start_time) + "\n")

    start_120 = dt.datetime.now()
    print("\n120 non RSA safe b bit prime number : ")
    print(prime_generator(120, s))
    print("Time Taken  : " + str(dt.datetime.now() - start_120) + "\n")

    start_200 = dt.datetime.now()
    print("\n200 non RSA safe b bit prime number : ")
    print(prime_generator(200, s))
    print("Time Taken  : " + str(dt.datetime.now() - start_200) + "\n")

    start_500 = dt.datetime.now()
    print("\n500 non RSA safe b bit prime number : ")
    print(prime_generator(500, s))
    print("Time Taken  : " + str(dt.datetime.now() - start_500) + "\n")

    return


""""This method runs all the code needed for the second part of the homework."""
def homework_part_2():
    print("HOMEWORK QUESTION 2")
    print("\n\nPollard Rho Factoring")
    print("Example from book")
    print("Factoring numbers :  262063, 9420457, and 181937053")
    book_num_1 = 262063
    book_num_2 = 9420457
    book_num_3 = 181937053
    book_array = [book_num_1, book_num_2, book_num_3]
    x_1 = 10

    for book in book_array:
        start_time = dt.datetime.now()
        print(pollard_rho(book, x_1))
        print("Time taken for : " + str(book) + "  was :  " + str(dt.datetime.now() - start_time))


    print("\n\nPollard Rho Factoring w/ Brents")
    print("Example from book")
    print("Factoring numbers :  262063, 9420457, and 181937053")


    for k in range(5, 155, 10):
        for book in book_array:
            if k == 55: break
            start_time = dt.datetime.now()
            print(pollard_rho_brents(book, x_1, k))
            print("Time taken for : " + str(book) + "  was :  " + str(dt.datetime.now() - start_time) +
                  "  with k value = " + str(k) + "\n")
        print()
    return


"""This method runs all the code needed for the 3rd part of the homework."""
def homework_part_3():
    print("\n\nPollard Rho Discrete Logs")
    p = 458009
    alpha = 2
    beta = 56851
    order = 57251
    print(pollard_rho_discrete_log(p, order, alpha, beta))

    print("\n\nPollard Rho Discrete Logs Experiment")
    for ran in range(50, 80, 10):
        print("\nExperiment run on " + str(ran))
        G = RSA_safe_prime_generator(ran, 9)
        alpha = random.randint(2, G-1)
        exponent = random.randint(2, G-1)
        beta = pow(alpha, exponent, G)

        start = dt.datetime.now()
        print(pollard_rho_discrete_log(G, G-1, alpha, beta))
        print("Total time for PLDL : " + str(dt.datetime.now() - start) + "\n")

        if ran <= 40:
            start = dt.datetime.now()
            print(shanks(G, G-1, alpha, beta))
            print("Total time for Shanks : " + str(dt.datetime.now() - start) + "\n")

        print("Answer : "+str(exponent) + "\n\n")
    return


main()


