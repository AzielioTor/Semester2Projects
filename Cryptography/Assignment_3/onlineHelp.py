def step_xab(x, a, b, alpha, beta, n, Z):
    s = x % 3

    # S1
    if s == 1:
        x = x * beta % Z
        b = (b + 1) % n

    # S2
    if s == 0:
        x = pow(x, 2, Z)
        a = 2 * a % n
        b = 2 * b % n

    # S3
    if s == 2:
        x = x * alpha % Z
        a = (a + 1) % n

    return x, a, b



def naturals_from(n):
    while True:
        yield n
        n += 1


def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return (g, x - (b // a) * y, y)


def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception("there's no inverse of %d on %d" % (a, m))
    else:
        return x % m


# Pollard's Rho algorithm for discrete logarithm (HAC 3.60).
# Returns the dlog of beta on the basis alpha and field Z
# n is the order of the field Z
def pollard_rho(alpha, beta, n, Z):
    x = {0: 1}
    a = {0: 0}
    b = {0: 0}

    # returns x, a, b for a given i using memoization
    def get_xab(i):
        if i not in x:
            _x, _a, _b = get_xab(i - 1)
            x[i], a[i], b[i] = step_xab(_x, _a, _b, alpha, beta, n, Z)
        return x[i], a[i], b[i]

    print
    "i\tx_i\ta_i\tb_i\tx_2i\ta_2i\tb_2i"
    for i in naturals_from(1):
        x_i, a_i, b_i = get_xab(i)
        x_2i, a_2i, b_2i = get_xab(2 * i)

        print
        "%d\t%d\t%d\t%d\t%d\t%d\t%d" % (i, x_i, a_i, b_i, x_2i, a_2i, b_2i)
        if x_i == x_2i:
            r = (b_i - b_2i) % n

            if r == 0:
                return False
            else:
                return modinv(r, n) * (a_2i - a_i) % n


# Example on HAC 3.61
alpha = 89
beta = 618
n = 101
Z = 809
l = pollard_rho(alpha, beta, n, Z)
print(l)






"""" OLD MILLER RABIN CODE """

"""
Main portion of the Miller Rabin primality test.
Had to split it out from the main part as to not have to 
    constantly recalculate `r`, `s`, and all the `a` values.
INPUT: n = the prime number we're testing
       a = number we're testing for a
       r & s = numbers needed to help with the calculations
OUTPUT: 1 if number is probably prime, 0 if number is DEFINITELY composite
"""
def miller_rabin_main(n, a, r, s):
    # Step 2.2
    y = sm(a, r, n)  # Square and Multiplication algorithm
    # Step 2.3
    if y != 1 and y != n - 1:
        j = 1
        while j <= s - 1 and y != n - 1:
            y = pow(y, 2, n)
            if y == 1:
                return 0  # "Composite"
            j = j + 1
        if y != n - 1:
            return 0  # "Composite"
    return 1  # "Probably Prime"

"""
Auxillary function for miller_rabin to set up the numbers
    needed to run the main miller_rabin method.
INPUT: The prime number we are testing primality for.
OUTPUT: Array containing the 1s and 0s for every a tested.
"""
def miller_rabin_aux(prime):
    results = []  # Create array to store results of miller_rabin on all 'a'
    r = prime - 1
    s = 0
    while r % 2 == 0:
        s = s + 1
        r = (r / 2)
    for a in range(2, prime - 1):
        results.append(miller_rabin_main(prime, a, r, s))
    return results