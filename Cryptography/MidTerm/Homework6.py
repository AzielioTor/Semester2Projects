"""
Aziel Shaw - 309000893
09/03/2018
Advanced Cryptography - Homework 5 - Midterm
"""


##### QUESTION 1 #####
def question_1_a():
    p = 71
    results = []
    for x in range(0, p):
        for y in range(0, p):
            z = function(x) % p
            target = pow(y, 2, p)
            if z == target:
                if z not in results:
                    results.append((x, z))
                else:
                    print()

    results.append(("infinity", "infinity"))
    for r in results:
        print(r)
    print("Length of results:", len(results))
    return results


def function(x):
    return pow(x, 3) + x + 28

def get_max_order(results):
    p = 71
    counters = []
    for r in results:
        counter = 0
        point = r
        res = []
        res.append(r)
        while (0, 0) != point and point not in res:
            print(point)
            point = point_add(point, p)
            res.append(point)
            counter = counter + 1
        counters.append((counter, r))
        print(counter, r)
    # Print results
    for c in counters:
        print(c)
    # print("Max counter", max(counters))
    return

def point_add(point, p):
    x = point[0]
    y = point[1]

    lamb = ((3 * pow(x, 2)) + 1) // (2 * y)
    new_x = pow(lamb, 2) - x - x
    new_y = (lamb * (x - new_x)) - y

    return (new_x % p, new_y % p)


##### MAIN METHOD #####
def main():
    results = question_1_a()
    get_max_order(results)

    return


main()
