"""
Aziel Shaw - 309000893
Advanced Cryptography Homework 6
09/04/2018
"""


def inv_mod_p(x, prime):
    """
    Compute an inverse for x modulo p, assuming that x
    is not divisible by p.
    """
    if x % prime == 0:
        raise ZeroDivisionError("Impossible inverse")
    return pow(x, prime - 2, prime)


def ec_point_add(P, Q, prime):
    lamb = -1

    if P[0] == Q[0] and P[1] == Q[1]:
        lamb = ((3 * pow(P[0], 2)) + a) * (inv_mod_p(2 * P[1], prime))
    else:
        lamb = (Q[1] - P[1]) * (inv_mod_p(Q[0] - P[0], prime))

    x_3 = pow(lamb, 2) - P[0] - Q[0]
    y_3 = (lamb * (P[0] - x_3)) - P[1]

    R = (x_3 % p, y_3 % p)

    return R


# QUESTION 6.18 PART a
def NAF(E):
    #Conver number to binary
    result = bin(E)
    i = 0
    z = []
    while E > 0:
        if E % 2 == 1:
            z.append(2 - (E % 4))
            E = E - z[i]
        else:
            z.append(0)
        E = E//2
        i = i + 1
    z.reverse()
    return z


print(NAF(87))


# QUESTION 6.18 PART b
def algorithm_6_5(P, c, prime):
    print("i", " ci", "   Q")
    Q = ('inf', 'inf')
    for i in range(len(c)-1, -1, -1):
        if(Q[0] == 'inf'): Q = P
        Q = ec_point_add(Q, Q, prime) # point double
        if(c[i] == 1):
            Q = ec_point_add(Q, P, prime)
        elif(c[i] == -1):
            Q = ec_point_add(Q, (P[0], -P[1]), prime)
        print(i, c[i], Q)
    return Q

p = 127
a = 1
b = 26

print("QUESTION 6.18")
P = (2, 6)
c = NAF(87)
c.reverse()
algorithm_6_5(P, c, p)


# QUESTION 6.17
p = 31
a = 2
b = 7
print("\n\nQUESTION 6.17")

P = (2, 9)
Q = P
for i in range(0, 7): # adding P to P 8 times
    Q = ec_point_add(P, Q, p)
print(Q)




# QUESTION 7.7
print("\n\nQUESTION 7.7")
# q = 101 # EXAMPLE NUMBERS
# p = 7879
# alpha = 170
# a = 75
# beta = 4567
# sha1 = 22
# k = 50
q = 101 # HOMEWORK NUMBERS
p = 7879
alpha = 170
a = 75
beta = 4567
sha1 = 52
k = 49
print("q = ", q)
print("p = ", p)
print("alpha = ", alpha)
print("a = ", a)
print("beta = ", beta)
print("sha1 = ", sha1)
print("k = ", k)

kinv = inv_mod_p(k, q)
print("kinv = ", kinv)

gamma = pow(alpha, k, p) % q
print("gamma = ", gamma)

delta = ((sha1 + (a * gamma)) * kinv) % q
print("delta = ", delta)

deltainv = inv_mod_p(delta, q)
print("deltainv = ", deltainv)

e1 = (sha1 * deltainv) % q
print("e1 = ", e1)

e2 = (gamma * deltainv) % q
print("e2 = ", e2)

result1 = (((alpha**e1) * (beta**e2)) % p) % q
result2 = gamma
print("result1 =", result1, " result2 =", result2)
print("finalresult =", result1 == result2)



# Question 6.17 partb
import math
def EC(x, p):
    return ((pow(x, 3)) + (2 * x) + 7) % p
    #return ((pow(x, 3)) + x + 6) % p

def pointcompress(x, i, p):
    z = EC(x, p)
    y = math.sqrt(z) % p
    if y % 2 == i % 2:
        return x, int(y)
    else:
        return x, int(p - y)

def decryptC(c):
    print("\n\n")
    m = 8
    p = 31

    x = pointcompress(c[0][0], c[0][1], p)
    print("point compress: ", x)
    P = x
    for i in range(0, m):
        x = ec_point_add(P, x, p)
    x = (x[0]%p, x[1]%p)
    print("point add: ", x)
    print(c[1], " * ", x[0], "^-1 mod", p)
    result = (c[1] * inv_mod_p(x[0], p)) % p
    print("Result is...")
    print(result)
c1 = ((18, 1), 21)
c2 = ((3, 1), 18)
c3 = ((17, 0), 19)
c4 = ((28, 0), 8)
carray = [c1, c2, c3, c4]
for c in carray:
    decryptC(c)
