from pyspark import SparkContext, SparkConf


# conf = SparkConf.setAppName("Hello World").setMaster(local)
sc = SparkContext("local", "first app")

rdd = sc.parallelize([1, 2, 3, 4])
print rdd.collect()
rdd2 = rdd.map(lambda x: x * 2)
print rdd2.collect()
rdd3 = rdd2.map(lambda x: pow(x, 2) // 2)
print rdd3.collect()