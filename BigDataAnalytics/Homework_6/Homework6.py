#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Aziel Shaw - 309000893
09/03/2018
Big Data Analytics Homework 6
"""


# Imports
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing as pp
from sklearn import linear_model
from sklearn.metrics import roc_auc_score
from sklearn.metrics import classification_report
from sklearn import metrics
from sklearn import svm


##### PREPROCESSING #####
def preprocessing(data):
#   Cast the _age_ to `float`
    age = data['age']
    x = np.array(age)
    age = x.astype(float)
    data['age'] = age
#   Cast the _hours-per-week_ to `float`
    hour = data['hours-per-week']
    x = np.array(hour)
    hour = x.astype(float)
    data['hours-per-week'] = hour
#   Remove any missing values from the dataframe
    data.dropna()
    data = consolidate_data(data)
#   Discretize the `age` variable with `cut()`
    num_bins = 20
    age_bins = pd.cut(data['age'], num_bins)
    data['age-bins'] = age_bins
#   Create bins for `hours-per-week` using `cut()`
    hours_bins = pd.cut(data['hours-per-week'], num_bins)
    data['hours_bins'] = hours_bins
#   Cross the numerical features `age` and `hours-per-week` into `age-hours`
    ages = data['age'].tolist()
    hours = data['hours-per-week'].tolist()
    agehours = []
    for i in range (0, len(ages)):
        curr_age = ages[i]
        curr_hor = hours[i]
        agehours.append(curr_age * curr_hor) 
    data["AgeAndHours"] = agehours

    age_hours_bins = pd.cut(data["AgeAndHours"], num_bins)
    data["AgeAndHours-bins"] = age_hours_bins

    le = pp.LabelEncoder()
    data = data.apply(le.fit_transform)

    y_axis = pd.DataFrame(data['income'])
    
    columns_to_drop = ['eduaction', 'income', 'hours_bins', 'age-bins', 
                       'AgeAndHours-bins', 'AgeAndHours']
    x_axis = pd.DataFrame(data).drop(columns_to_drop, axis=1)
    return x_axis, y_axis



##### REGRESSION #####
##### QUESTION 1, 2, & 3 #####
def Regression_Model(data):
    train_x, train_y, test_x,  test_y = data[0], data[1], data[2], data[3]
#     - Linear Regression
    lin_reg = linear_model.LinearRegression()
    lin_reg.fit(train_x, train_y)
#   Get rsquared
    lin_r = lin_reg.score(test_x, test_y)
    print("Linear Regression R^2: " + str(lin_r))
#   Get params
    lin_params = lin_reg.get_params()
#   Print rsquared and params
    print("Linear Regression linear params: " + str(lin_params))
    print("Linear Regression Coefs: " + str(lin_reg.coef_))
    
    
#     - Ridge Regression
    ridge = linear_model.Ridge()
#   Fit to training data
    ridge.fit(train_x, train_y)
#   Get score
    ridge_score = ridge.score(test_x, test_y)
#   Get coefs
    ridge_params = ridge.coef_
#   Print score and coefs
    print("\nRidge Regression Score: " + str(ridge_score))
    print("Ridge Regression coefs: " + str(ridge_params))
    
    
    
#     - Lasso Regression
    lasso = linear_model.Lasso()
#   Fit to training data
    lasso.fit(train_x, train_y)
#   Get score for lasso
    lasso_score = lasso.score(test_x, test_y)
#   Get coefs
    lasso_coef = lasso.coef_
#   Print details
    print("\nLasso score: " + str(lasso_score))
    print("Lasso coefs: " + str(lasso_coef)) 
    
    return



##### CLASSIFICATION #####
##### QUESTION 4, 5, & 6 #####
def Classification_Model(data):
#     - Start data
    train_x, train_y, test_x,  test_y = data[0], data[1], data[2], data[3]
#     - Logistic Regression
    log_reg = linear_model.LogisticRegression()
#   Fit model to training data
    log_reg.fit(train_x, train_y)
#   Predict with test data
    log_reg_results = log_reg.predict(test_x)
#   Get Accuracy for logistic regression
    log_accuracy = log_reg.score(test_x, test_y)
    print("Logistic Regression Accuracy: " + str(log_accuracy))
#       Print classification report
    print(classification_report(test_y, log_reg_results))
#   PLOT ROC CURVE
    plot_ROC(test_y, log_reg_results, "Logistic Regression")
    
    
#   Perceptron
    perceptron = linear_model.Perceptron()
#   Fit model to Perceptron
    perceptron.fit(train_x, train_y)
#   Predict data using perceptron
    preceptron_results = perceptron.predict(test_x)
#   Get perceptron accuracy
    perceptron_score = perceptron.score(test_x, test_y)
    print("\n\n\nPerceptron Results: " + str(perceptron_score))
#   Print classification report
    print(classification_report(test_y, preceptron_results))
#   PLOT ROC CURVE
    plot_ROC(test_y, preceptron_results, "Perceptron")
    
    
#   Linear SVM
    linear_SVM =  svm.SVC()
#   Fit data to SVM model
    linear_SVM.fit(train_x, train_y)
#   Get SVM Results
    linear_SVM_results = linear_SVM.predict(test_x)
#   Get SVM accuracy
    linear_results = linear_SVM.score(test_x, test_y)
    print("\n\n\nLinear SVM results: " + str(linear_results))
#   Print classification report which contains Precision, Recall, F1
    print(classification_report(test_y, linear_SVM_results))
#   PLOT ROC CURVe
    plot_ROC(test_y, linear_SVM_results, "Linear SVM")
    return



##### HELPER METHODS #####
def plot_ROC(actual, target, name):
    print("Plotting curve ROC for " + name)
#       ROC VURCE
#   Compute data
    fpr, tpr, thresholds = metrics.roc_curve(actual, target)
#   GET AUC FROM SKLEARN
    auc = metrics.auc(fpr, tpr)
    print(auc)
#   Plot false vs true positivies
    plt.plot(fpr, tpr, color='xkcd:warm purple')
    plt.plot([0, 1], [0, 1], linestyle='-.', lw=2, color='xkcd:hot green', alpha=.8)
    plt.title(name)
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
#   Show plot
    plt.show()
    
    area = roc_auc_score(actual, target)
    print("(AUC) Area under this ROC curve: " + str(area))
    return

def consolidate_data(data):    
    ### eduaction
    HS_Drop = 'HS-Drop'
    data['eduaction'] = data['eduaction'].replace(' 11th', HS_Drop)
    data['eduaction'] = data['eduaction'].replace(' 9th', HS_Drop)
    data['eduaction'] = data['eduaction'].replace(' 7th-8th', HS_Drop)
    data['eduaction'] = data['eduaction'].replace(' 5th-6th', HS_Drop)
    data['eduaction'] = data['eduaction'].replace(' 10th', HS_Drop)
    data['eduaction'] = data['eduaction'].replace(' 1st-4th', HS_Drop)
    data['eduaction'] = data['eduaction'].replace(' 12th', HS_Drop)
    data['eduaction'] = data['eduaction'].replace(' Assoc-voc', ' Assoc')
    data['eduaction'] = data['eduaction'].replace(' Assoc-acdm', ' Assoc')
    
    ### Marital
    data['marital-status'] = data['marital-status'].replace(' Married-civ-spouse', ' Married')
    data['marital-status'] = data['marital-status'].replace(' Married-spouse-absent', ' Married')
    
    ### Income
    data['income'] = data['income'].replace(' <=50K', ' -1')
    data['income'] = data['income'].replace(' >50K', ' 1')
    data['income'] = data['income'].replace(' <=50K.', ' -1')
    data['income'] = data['income'].replace(' >50K.', ' 1')
    
    ### Return
    return data


##### MAIN METHOD #####
def main():
    # Read in data and add columns
    training = pd.read_csv("adult.data.txt", header=None)
    testing  = pd.read_csv("adult.test.txt", header=None)
    column_headers = ['age', 'workclass', 'fnlwgt', 'eduaction', 
                      'eduaction-num', 'marital-status', 'occupation', 
                      'relationship', 'race', 'sex', 'capital-gain', 
                      'capital-loss', 'hours-per-week', 'native-country', 
                      'income']
    training.columns = column_headers
    testing.columns  = column_headers
    # preprocessing
    
    train_x, train_y = preprocessing(training)
    test_x,  test_y  = preprocessing(testing)
    
    # Run regression and classification
    variables = [train_x, train_y, test_x,  test_y]
    print("STARTING REGRESSION\n")
    Regression_Model(variables)
    print("\nFINISHING REGRESSION")
    print("\n\n\nSTARTING CLASSIFICATION")
    Classification_Model(variables)
    print("FINISHING CLASSIFICATION")
    return (train_x, train_y, test_x,  test_y)

a, b, c, d = main()
