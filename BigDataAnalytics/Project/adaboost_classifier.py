#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 31 19:34:33 2018

@author: tushariyer
"""
import warnings
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

warnings.filterwarnings("ignore")


# -----------------------------------------------------------------------------
#       HELPER FUNCTIONS
# -----------------------------------------------------------------------------

# ------------------------------
#       GET ERROR RATE
# ------------------------------
def get_error_rate(pred, Y):
    return sum(pred != Y) / float(len(Y))


# ------------------------------
#       PLOT ERROR RATE
# ------------------------------
def plot_error_rate(generic_train, generic_test):
    dataframe_error = pd.DataFrame([generic_train, generic_test]).T
    dataframe_error.columns = ['Training', 'Test']
    
    # Plot details
    axes = dataframe_error.plot(linewidth = 3, figsize = (8,6), color = ['orange', 'blue'], grid = True)
    axes.set_xlabel('Iterations')
    axes.set_ylabel('Error Rate')
    axes.set_title('Error Rate vs Number of Iterations')
    
    plt.axhline(y = generic_test[0], linewidth = 2, color = 'darkgreen', ls = 'dashed')


# ------------------------------
#    ADABOOST CLASSIFIER MAIN
# ------------------------------
def adaboost_classifier(train_x, train_y, test_x, test_y, M, dtree):
    
    num_train, num_test = len(train_x), len(test_x)  # Count the number of train/test instances
    weights = np.ones(num_train) / num_train  # Original weights
    train_predictions, test_predictions = [np.zeros(num_train), np.zeros(num_test)]  # Original predictions
    
    for i in range(M):
        # Fit the original weights
        dtree.fit(train_x, train_y, sample_weight = weights)
        train_iterations = dtree.predict(train_x)
        test_iterations = dtree.predict(test_x)
        
        # Indicator function
        original_missed_vals = [int(x) for x in (train_iterations != train_y)]  # Indicator
        updated_missed_vals = [x if x==1 else -1 for x in original_missed_vals]  # Update weights
        
        error_of_missed = np.dot(weights,original_missed_vals) / sum(weights)  # Error
        
        alpha_m = 0.5 * np.log( (1 - error_of_missed) / float(error_of_missed))  # Alpha
        
        weights = np.multiply(weights, np.exp([float(x) * alpha_m for x in updated_missed_vals]))  # New Weights
        
        # Add to prediction
        train_predictions = [sum(x) for x in zip(train_predictions, [x * alpha_m for x in train_iterations])]
        test_predictions = [sum(x) for x in zip(test_predictions, [x * alpha_m for x in test_iterations])]
    
    train_predictions, test_predictions = np.sign(train_predictions), np.sign(test_predictions)
    
    return get_error_rate(train_predictions, train_y), get_error_rate(test_predictions, test_y), test_predictions

