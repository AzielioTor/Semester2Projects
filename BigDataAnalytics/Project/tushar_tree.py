#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  4 12:11:09 2018

@author: tushariyer
@author: azielshaw
"""
import warnings
import pandas as pd
from math import log
from types import ListType
from collections import Counter
warnings.filterwarnings("ignore")


# ------------------------------
#     DECISION TREE CLASS
# ------------------------------
class DecisionTree():
    tree = {}

    def learn(self, training_data, attribute_list, target_attribute, target_values):
        self.tree = build_decision_tree(training_data, attribute_list, target_attribute, target_values)


# ------------------------------
#          NODE CLASS
# ------------------------------
class Node():
    value = ""
    kids = []

    def __init__(self, val, dictionary):
        self.value = val
        if (isinstance(dictionary, dict)):
            self.kids = dictionary.keys()


# ------------------------------
#         GET MAJORITY
# ------------------------------
def get_majority(target_values):
    data = Counter(target_values)
    majority = data.most_common(1)

    return majority[0][0]


# ------------------------------
#       CALCULATE ENTROPY
# ------------------------------
def entropy(attribute_list, full_data, target_attribute):
    frequencies = {elem:0 for elem in attribute_list}  # Populate base dict with 0's
    dataEntropy = 0.0
    
    for names in full_data:
        # Avoid lists being passed in
        if type(names) is str:
            if names in frequencies:
                frequencies[names] += 1.0
            elif names not in frequencies:
                frequencies[names] = 1.0
    
    data_length = len(full_data)
    freq_vals = frequencies.values()
    for frequency in freq_vals:
        if frequency > 0:
            dataEntropy += (-frequency/data_length) * log(frequency/data_length, 2) 
        
    return dataEntropy


# ------------------------------
#       INFORMATION GAIN
# ------------------------------
def information_gain(attribute_list, full_data, picked_attribute, target_attribute):

    frequencies = {elem:0 for elem in attribute_list}  # Populate base dict with 0's
    subsetEntropy = 0.0
    
    for entry in full_data:
        # Avoid lists being passed in
        if type(entry) is str:
            if entry not in frequencies:
                frequencies[entry] = 1.0
            else:
                frequencies[entry] += 1.0
    
    freq_keys = frequencies.keys()
    freq_vals = frequencies.values()
    freq_vals_sum = float(sum(freq_vals))
    
    for val in freq_keys:
        if freq_vals_sum <= 0:
            break
        else:
            valProb = frequencies[val] / freq_vals_sum
            dataSubset = [entry for entry in full_data if entry == val]
            subsetEntropy += valProb * entropy(attribute_list, dataSubset, target_attribute)

    return (entropy(attribute_list, full_data, target_attribute) - subsetEntropy)


def choose_attribute(full_data, attribute_list, target_attribute):
        max_info_gain = 0
        for picked in attribute_list:
            max_info_gain = max(information_gain(attribute_list, full_data, picked, target_attribute), max_info_gain)
            if information_gain(attribute_list, full_data, picked, target_attribute) == max_info_gain:
                return picked
        return 'attribute not found'
    
# ------------------------------
#          GET VALUES
# ------------------------------
def get_values(full_data, picked_attribute):
    return full_data[picked_attribute].unique()


# ------------------------------
#           GET DATA
# ------------------------------
def get_data(full_data, attribute_list, best_pick, val):

    new_data = [[]]
    index = attribute_list.index(best_pick)

    for entry in full_data:
        if (entry[index] == val):
            new_entry = []
            for i in range(0,len(entry)):
                if(i != index):
                    new_entry.append(entry[i])
            new_data.append(new_entry)

    new_data.remove([])    
    return new_data


# ------------------------------
#     BUILD DECISION TREE
# ------------------------------
def build_decision_tree(full_data, attribute_list, target_attribute, target_values):
    
    default_majority = get_majority(target_values)  # full_data, attribute_list, target_attribute)
    decision_tree = {}  # Initialize an empty tree
    
    # Check for empty list
    if (type(full_data) is ListType) or type(full_data) is list:
        if not full_data:
            return default_majority
        
    # Check for empty dataframe
    if isinstance(full_data, pd.DataFrame) and full_data.empty:
        return default_majority
        
    elif target_values.count(target_values[0]) == len(target_values):
        return target_values[0]
    
    else:
        # Calculate the best pick
        best_pick = choose_attribute(full_data, attribute_list, target_attribute)
        decision_tree = {best_pick:{}}  # Add the best pick to the tree
    
        for each_value in get_values(full_data, best_pick):
            new_data = get_data(full_data, attribute_list, best_pick, each_value)
            next_attribute = attribute_list[:]
            next_attribute.remove(best_pick)
            subtree = build_decision_tree(new_data, next_attribute, target_attribute, target_values)
            decision_tree[best_pick][each_value] = subtree
    
    return decision_tree