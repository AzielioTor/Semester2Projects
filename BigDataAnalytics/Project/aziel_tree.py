#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  5 13:43:17 2018

@author: Azielio
"""
import pandas as pd
from preprocessing import prune_data as prune
from sklearn.model_selection import train_test_split as tts
from classifiers_baseline import get_baseline as base
from math import log



class decision_tree():
    tree = {}
    
    def learn(self, training_set, attributes, target, target_values):
        print("Learning from training set...")
        self.tree = build_tree(training_set, attributes, target, target_values)
        return
    

def build_tree(training_set, attributes, target, target_values):
    tree = {}
    
    
    
    return tree

    
    
def entropy(attribute_list, full_data, target_attribute):
    frequencies = {elem:0 for elem in attribute_list}  # Populate base dict with 0's
    dataEntropy = 0.0
    
    for names in full_data:
        # Avoid lists being passed in
        if type(names) is str:
            if names in frequencies:
                frequencies[names] += 1.0
            elif names not in frequencies:
                frequencies[names] = 1.0
    
    data_length = len(full_data)
    freq_vals = frequencies.values()
    for frequency in freq_vals:
        if frequency > 0:
            dataEntropy += (-frequency/data_length) * log(frequency/data_length, 2) 
        
    return dataEntropy

    
def information_gain(attribute_list, full_data, picked_attribute, target_attribute):
    frequencies = {elem:0 for elem in attribute_list}  # Populate base dict with 0's
    subsetEntropy = 0.0
    
    for entry in full_data:
        # Avoid lists being passed in
        if type(entry) is str:
            if entry not in frequencies:
                frequencies[entry] = 1.0
            else:
                frequencies[entry] += 1.0
    
    freq_keys = frequencies.keys()
    freq_vals = frequencies.values()
    freq_vals_sum = float(sum(freq_vals))
    
    for val in freq_keys:
        if freq_vals_sum <= 0:
            break
        else:
            valProb = frequencies[val] / freq_vals_sum
            dataSubset = [entry for entry in full_data if entry == val]
            subsetEntropy += valProb * entropy(attribute_list, dataSubset, target_attribute)

    return (entropy(attribute_list, full_data, target_attribute) - subsetEntropy)


def get_values(full_data, picked_attribute):
    return full_data[picked_attribute].unique()


def get_data(full_data, attribute_list, best_pick, val):
    new_data = [[]]
    index = attribute_list.index(best_pick)

    for entry in full_data:
        if (entry[index] == val):
            new_entry = []
            for i in range(0,len(entry)):
                if(i != index):
                    new_entry.append(entry[i])
            new_data.append(new_entry)

    new_data.remove([])    
    return new_data


def choose_attribute(full_data, attribute_list, target_attribute):
    max_info_gain = 0
    for picked in attribute_list:
        max_info_gain = max(information_gain(attribute_list, full_data, picked, target_attribute), max_info_gain)
        if information_gain(attribute_list, full_data, picked, target_attribute) == max_info_gain:
            return picked
    return 'attribute not found'
    
   



def run_decision_tree(train_x, train_y, test_x, test_y):
    training_set = pd.concat([train_x, train_y], axis=1)
    testing_set = pd.concat([test_x, test_y], axis=1)
    target_values = training_set.iloc[:,-1].tolist()
    
    attributes = training_set.columns.tolist()
    target = 'readmitted'

    
    tree = decision_tree()
    tree.learn(training_set, attributes, target, target_values)
    results = []

#    for entry in testing_set:
#        tempDict = tree.tree.copy()
#        result = ""
#        while(isinstance(tempDict, dict)):
#            root = Node(tempDict.keys()[0], tempDict[tempDict.keys()[0]])
#            tempDict = tempDict[tempDict.keys()[0]]
#            index = attributes.index(root.value)
#            value = entry[index]
#            if(value in tempDict.keys()):
#                result = tempDict[value]
#                tempDict = tempDict[value]
#            else:
#                result = "Null"
#                break
#        if result != "Null":
#            results.append(result == entry[-1])
#
#        print results


def final_prep(df):
    # Split the target attribute into its own dataframe
    target = ['readmitted']
    df_y = df['readmitted']
    df_x = df.drop(target, axis=1)
    
    # Split into training and testing data
    train_x, test_x, train_y, test_y = tts(df_x, df_y, test_size=0.33, random_state=42)  
    
    print "\nData Spliting Complete"
    
    return [train_x, train_y, test_x, test_y]


def main():
    df_original = pd.read_csv("dataset_diabetes/diabetic_data.csv")  # Import data
    
    threshold = 5  # Toggle for severity of attribute pruning. Does not affect protected attributes
    print "\nThreshold set at:",threshold
    
    print "\nBeginning Preprocessing"
    df_preprocessed = prune(df_original, threshold)
    print "\nPreprocessing Complete"
    
    print "\nOriginal Dimensions:", df_original.shape
    print "Preprocessed Dimensions:", df_preprocessed.shape
    
    # Prep the data for the baseline tests
    data = final_prep(df_preprocessed)
    
    # Get baseline
    print "\nGetting baseline calculations\n"
    base(data[0], data[1], data[2], data[3])
    
    # Prep Adaboost
#    print "\nPreparing for AdaBoost"
#    prep_boosting(data[0], data[1], data[2], data[3])
    run_decision_tree(data[0], data[1], data[2], data[3])
    return

# ------------------------------
#              INIT
# ------------------------------
if __name__ == "__main__":
    print("starting")
    main()  # Run main routine
