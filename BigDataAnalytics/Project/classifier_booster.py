#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  9 13:43:30 2018

@author: Aziel Shaw
@author: Tushar Iyer
"""
import pandas as pd
import collections
from sklearn.model_selection import train_test_split as tts



"""
Given a test set of data, boost the results.
test_x = the dataframe of the data you're trying to boost
test_y = dataframe of the target variable
dtree = the decision tree to run
min_delete = the minimum bound for boosting. Default min bound is 80% freq
max_delete = the maximum bound for boosting. Default max bound is 100% freq
columns_to_check = a list of the desired columns to boost, defaults to None so
    all columns are checked
"""
def booster(test_x, results_test, min_delete=0.80, 
            max_delete=1.0, columns_to_check=None):
    test_x = test_x.reset_index(drop=True)
    # Get indexes of failures
    failure_indexes = []
    for i in range(0, len(results_test) - 1):
        if results_test[i] == False:
            failure_indexes.append(i)
    # Append failed rows
    rows = pd.DataFrame(columns = test_x.columns.values)
    for i in failure_indexes:
        rows = rows.append(test_x.iloc[[i]])
    # Make dataframe with failed rows
    incorrect_data = pd.DataFrame(rows, columns=test_x.columns.values)
    # Get columns to check
    if columns_to_check == None:
        column_headers = list(incorrect_data.columns.values)
    else:
        column_headers = columns_to_check
    # Get length of dataframe of failed rows
    num_rows = len(incorrect_data)
    # Iterate through the columns and get frequency of items within 
    to_delete = []
    for column in column_headers:
        data = incorrect_data[column].tolist()
        attr, occur = getFrequency(data, num_rows)
        # Append column and value to delete
        if occur > min_delete and occur < max_delete:
            print(column, attr, occur)
            to_delete.append((column, attr))
    # Perform boosting NOTE: loop from top so we don't ruin indicies
    
    for i in range(len(results_test) - 1, -1, -1):
        if results_test[i] == False:
            for column, attr in to_delete:
                # If the value at specified index = the attribute then boost
                if test_x.iloc[[i]][column].values.tolist()[0] == attr:
                    print(i)
                    test_x = test_x.drop(i)
                    break
    return test_x



def boost(results, testing_data):
    testing_data = testing_data.reset_index(drop=True)
        # Store indices of all occurences of "FALSE"
    indices = [i for i, x in enumerate(results) if x == False]
    return indices
    
    # Get all transactions that resulted in a "FALSE"
    testing_data_false = [testing_data[i] for i in indices]
    
    threshold = 0.65
    vals_to_remove = []
    col_idx = []

    column_transposed = [list(i) for i in zip(*testing_data_false)]
    col_id = 0
    for column in column_transposed:
        unique_values = set(column)
        for value in unique_values:
            val_freq = float(column.count(value))/float(len(column))
            if val_freq > threshold:
                vals_to_remove.append(value)
                col_idx.append(col_id)
        col_id += 1
    
    print "COLIDX: ",len(col_idx)
    print "TSPOSE: ",len(column_transposed)
    for idx in col_idx:
        del column_transposed[idx]
    
    data_final = [list(i) for i in zip(*column_transposed)]
    
    return data_final



"""
This method takes a column from a dataframe (as a list) and returns the most 
    most common element inside that column, and the frequency that it shows up.
data = the dataframe column (as a list)
num_rows = the length of the rows you are checking
num_common = the top x amount of common items you want to check. (default = 1)
"""
def getFrequency(data, num_rows, num_common=1):
    # Instantiate Counter Column
    counter = collections.Counter(data)
    # Perform operations to count the most common element
    most_common = counter.most_common(num_common)
    num_occurance = most_common[0][1]
    most_common_value = most_common[0][0]
    rate_occurance = float(float(num_occurance) / float(num_rows))
    # Return the most common attribute for the given column, and the frequency 
    #     of which it shows up.
    return most_common_value, rate_occurance




def final_prep(df):
    # Split the target attribute into its own dataframe
    target = ['readmitted']
    df_y = df['readmitted']
    df_x = df.drop(target, axis=1)
    
    # Split into training and testing data
    train_x, test_x, train_y, test_y = tts(df_x, df_y, test_size=0.30, 
                                           random_state=42)  
    
    print "\nData Spliting Complete"
    
    return [train_x, train_y, test_x, test_y]






df_original = pd.read_csv("dataset_diabetes/diabetic_data.csv")
data = final_prep(df_original)
resu = "Results should have less than 30530 values"






results_test = [True, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False]#, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, False, False, False, True, False, True, True, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False, True, False, False, True, False, False, True, False, False, True, True, False, False, True, True, True, False, False, True, True, False, False]


#results = booster(data[2], results_test, 0.68)
print("Starting")
results = boost(results_test, data[2])
print("Completed Booster")





