% --------------------------------------------------------------------------------------------------
%                                          Project Report
% --------------------------------------------------------------------------------------------------
 
\documentclass[12pt]{article}
 
\def\BibTeX{\textsc{Bib}\TeX}
\usepackage{url}
\usepackage{balance}

\usepackage[margin=1in]{geometry} 
\usepackage{amsmath,amsthm,amssymb}
\usepackage{listings}
\usepackage{color}


\usepackage{graphicx}
\usepackage{placeins}
\graphicspath{ {/} }

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=Python,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}
 
\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
 
\newenvironment{theorem}[2][Theorem]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
\newenvironment{lemma}[2][Lemma]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
\newenvironment{exercise}[2][Exercise]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
\newenvironment{problem}[2][Problem]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
\newenvironment{question}[2][Question]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}
\newenvironment{corollary}[2][Corollary]{\begin{trivlist}
\item[\hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}]}{\end{trivlist}}

\newenvironment{solution}{\begin{proof}[Solution]}{\end{proof}}

\begin{document}

\title{Big Data Analytics - Project Report \vspace{150mm} \\ {\large CSCI 720 - Spring 2018}}
\author{Tushar Iyer and Aziel Shaw}
\date{\today}
\clearpage\maketitle
\thispagestyle{empty}

\newpage
\pagenumbering{arabic}

\tableofcontents
\listoftables
\listoffigures

\newpage
% ---------------------------------------------------------------------------------------------------
%                                               PROJECT DESCRIPTION
% ---------------------------------------------------------------------------------------------------
\section{Project Description}
\label{Project_Description}
Diabetes is a problem worldwide with more people being diagnosed each year~\cite{Diabetes}.
We propose a project will focus on analyzing data recorded over 10 years at 130 US hospitals~\cite{UCI}. The dataset consists of information on diabetes tests and treatments done at these hospitals along with details and intensities of the tests, results, and treatments. Our plan is to analyze this data to see there is a specific common variable or trend that is consistent with the readmission diagnosis.\newline

In theory, such a discovery would allow for faster diagnoses if a factor is present or if a set of symptoms present themselves in a given patient. Our hope is that if we can find trends within the people being diagnosed, we may find a way to use that to strengthen treatment and thereby reduce the number of people who need to be readmitted. This collection of data also includes details on how many treatments a patient has undergone and how many unique medications have been consumed. We plan on attempting to analyze those variables to find the common factor which results in a patient needing so much different medication. As a stretch goal for our project we would also like to see if we are able to perform any analysis on the patterns we find to further boost the classification.


% ---------------------------------------------------------------------------------------------------
%                                               DATA DESCRIPTION
% ---------------------------------------------------------------------------------------------------
\section{Data Description}
\label{Data_Description}
This dataset contains information gathered from 130 hospitals in the United States, in order to help identify attributes and results that make a patient more likely to have to be readmitted. The data contains 55 attributes relating to the medical history of both the patients and occurrences of each medical field. This data was retrieved from the databases based on the given criteria:
\begin{enumerate}
\item The type of hospital admission.
\item Whether or not the admission was part of a transfer from another medical facility.
\item The readmission diagnosis.
\item The duration of time spent inside the hospital.
\item Whether or not laboratory tests were performed during the encounter (As well as how much).
\item Whether or not medications were administered during the encounter (As well as how much).
\end{enumerate}
Despite the amount of detail present in the dataset for the various patients, the patients themselves are still anonymized, save for the ID number for the hospital encounter. \newline

\newpage
% ---------------------------------------------------------------------------------------------------
%                                               CRISP-DM
% ---------------------------------------------------------------------------------------------------
\section{CRISP-DM}
\label{crispdm}
This project was structured so that it followed the {\sc CRISP-DM} methodology. Each phase is addressed below:\newline

\subsection{Business Understanding}
\label{phaseOne}
\begin{enumerate}
\item {\bf Project Objectives}\newline
The project objective is to pick a dataset for which a data mining problem can be formed (Whilst adhering to the {\sc CRISP-DM} methodology). The project scope covers the mining and analysis of the data and any results or hypotheses that are confirmed/debunked will be presented as part of the results in Section \ref{results}. 

From the group's perspective, the objective is to take the picked dataset and see if any hypotheses are possible with a cursory analysis. Further analysis will then be done in an attempt to confirm or deny the hypotheses. After the data preparation phase is complete (Subsection \ref{phaseThree}), a baseline modeling will be performed with a variety of modeling techniques using the {\tt sklearn} library~\cite{sklearn_api}. 

The point of this is to establish how existing tools handle this data after the preprocessing phase. From this, the group will pick a specific type of data modeling and write their own classifier or regressors to try and outperform the {\tt sklearn} version. While the outcome goal of this project is still to try and find something meaningful, an added goal is to see if it is possible to create a model that works with this specific data better than others.

The dataset that was picked has to do with medical data for diabetic patients. It covers a ten year span, and the identified target attribute is the diagnosis of {\em whether or not readmission is necessary} for a given patient, given that patients' medical data.

\item {\bf Project Requirements}\newline
The main requirement is that the project adhere to the {\sc CRISP-DM} model. The requirements also include that each group will present their findings and write a report of the project as well.

\item {\bf Data Mining Problem Definition}\newline
From a precursory look at the data, the problem definition is as follows:\newline

{\em To what extent, can the data provided serve as a basis for an accurate diagnosis of whether or not readmission is necessary, with the understanding that such a solution may one day serve as a tool used by medical professionals to deliver a diagnosis faster than before whilst also reducing the amount of tests necessary for diagnosis.}

\item {\bf Preliminary Plan}\newline
The preliminary plan is to start by understanding the data given and what each attribute contributes to the overall picture. Data preparation will then be conducted in order to reduce the data such that no redundancies or extraneous data can alter the accuracy of the decision model. Several basic modeling techniques will be tested using the {\tt sklearn} library~\cite{sklearn_api} in order to ensure that the group is working with a model that works {\em for this specific data}. It is not to be confused that the selected model is better than others across the board, but rather only for this project.
\end{enumerate}

\subsection{Data Understanding}
\label{phaseTwo}
\begin{enumerate}
\item {\bf Data Collection}\newline
The description of the data is given in detail in Section \ref{Data_Description}. The dataset has to do with medical histories (Over a ten year period) for diabetic patients. The identified target attribute is the diagnosis of {\em readmission} for a given patient, given that patients' medical data. The three types of diagnoses are: {\tt No readmission required}, {\tt Within 30 Days}, and {\tt After at least a month}.

\item {\bf First Insights}\newline
The first insights into this dataset is that while the dataset contains mostly complete instances, there exist several attributes with lots of unknown data such as weight, payer code, and medical specialty. In attributes featuring various chemicals or medications, it has been found that a significant portion of the data (In excess of 75\% of the data available for those attributes) has the same value. This leads to the belief that such attributes will not help a model given that the value is uniform almost throughout the instances. However, the severity of the data preparation's pruning will better determine the relevance of these attributes.

\item {\bf Hypotheses Formed}\newline
It is worth noting before making predictions that {\em diabetes} is \underline{not} the target of this project. For each of these patients, the existence of diabetes has already been diagnosed. The only diagnosis here has to do with the {\em readmission} of a patient. 

Given that declaration, it is the opinion of the group that certain attributes are vital to the readmission diagnosis. {\em weight} and {\em insulin} are two such factors which are hypothesized to have a significant impact on whether a patient will require further treatment. The number of test procedures conducted is another attribute which is believed to have an effect, as more tests conducted over the same 10 year period is indicative of either repetitive symptoms or an effort to track progress on a long-term procedure.
\end{enumerate}

\newpage
\subsection{Data Preparation}
\label{phaseThree}
The data preparation stage of this project is shown in the {\tt preprocessing} script.
\begin{enumerate}
\item {\bf Threshold Definition}\newline
The preprocessing has been structured in such a way that a singular threshold value is passed in with the data. This threshold defines the severity of the pruning process. A lower threshold level will result in a higher level of pruning, and vice versa. 

\item {\bf Data Imputation}\newline
As mentioned in the {\em Data Collection} part of Subsection \ref{phaseTwo}, this data has attributes which seem to be imputed, as some of the values are marked as `{\tt ?}'. In any case, missing data is removed from the dataset to prevent any inconsistencies in the instances that make it to the modeling phase. 

\item {\bf Attribute Protection}\newline
Given that the severity of pruning can be varied, certain attributes are then protected against deletion. Attributes like {\em Age}, {\em Gender}, {\em Readmission}(The target) are evenly spread across their unique values and are quite necessary for the modeling stage. When pruning, every attribute which fits the requirements for deletion is checked against the protected list to prevent deleting them.

\item {\bf Histogram Analysis}\newline
Before plotting histograms, all the attributes undergo a unique variable screening. The threshold given is first used to create a mini-threshold, which also has a correlation to the severity level. The unique variable screening is then done for each attribute. Any attribute that has fewer unique values than the mini-threshold will most likely represent an attribute with great bias towards a single value. These attributes are pruned out in a preliminary elimination.

After this comes histogram analysis, where a histogram is plotted for the attributes that survived round one elimination. The {\tt bins} for the histograms are assigned automatically, and that data is returned. If an attribute has fewer bins than the original threshold, it is then checked against the protected list prior to being marked for round two elimination.

\item {\bf Attribute Pruning}\newline
Along with round two elimination, there are a couple attributes such as {\em Medical Encounter ID} and {\em Payer Code} which serve as useful only in the situation when you are requisitioning records from a specific hospital. They add no purpose to this project. These attributes join the pruning list generated by the histogram analysis.

The data preparation is concluded by performing the round two attribute elimination and then returning the data.

After receiving the cleaned and prepared data, it is then staged for the modeling phase by being split into a {\em training} set and {\em testing} set and then into {\tt X, Y} dataframes, where {\tt Y} is the target.
\end{enumerate}

\subsection{Modeling}
\label{phaseFour}
\begin{enumerate}
\item {\bf Baseline Testing}\newline
Up until now, the course has made use of the {\tt sklearn}~\cite{sklearn_api} module in {\sc python} to conduct modeling and data analysis. In order to see what type of modeling is best able to capture this data, multiple models are generated and tested with the preprocessed data. The scores and associated values are then compared to then see which performs best. 

Out of all tested, a {\em Decision Tree Classifier} was shown to have average performance on this data, but saw a significant increase in accuracy when aided by an upper bound checker called {\em High Screening}. 

\item {\bf Model Selection}\newline
Based on the findings from the previous stage, the selected model was a {\em High Screening} that worked on a {\em Decision Tree Classifier}. 

\item {\bf Model Creation}\newline
Since the boosting was key to the accuracy being brought to a decent level, it was given priority during model creation. 

A custom {\sc High-Screening} method was first created to work on top of the {\tt sklearn}~\cite{sklearn_api} decision tree classification, to prove that the purging of extremely variant attributes was possible and indeed significant enough to make a difference. After this was working, a custom {\sc Decision Tree Classifier} was written. The attempt here was to achieve a higher end accuracy than possible with the {\tt sklearn} module.

\item {\bf High-Screening}\newline
For the boosting, a base classification is first generated with the decision tree classifier and no weights. The results and the data are then sent to the {\sc High Screening} which will then look for attributes with an extreme amount of variance. These attributes are ones which have different entries for each transaction. There is no correlation between those kind of attributes and the final diagnosis, so they can be purged in an attempt to `boost' the accuracy. The base decision tree classification is then redone (now with a list of weights), to see if the accuracy of the classifier is {\em boosted}.
\end{enumerate}

\newpage
\subsection{Evaluation}
\label{phaseFive}
\begin{enumerate}
\item {\bf Model Evaluation}\newline
Given that the {\sc Decision Tree Classifier} is a weak learning algorithm, the accuracy was not expected to be extremely high. {\tt sklearn}'s baseline decision tree only resulted in an accuracy of {\tt 47.30\%} for the preprocessed data. The custom {\sc Decision Tree Classifier} yielded an average accuracy of {\tt 57.25\%}, outperforming the {\tt sklearn} module by a factor of {\tt 1.21} (An almost 10\% increase in diagnosis accuracy). After the {\sc High Screening}, the {\sc Decision Tree Classifier} showed a final accuracy rate of {\tt 60.20\%} (A {\tt 2.94\%} increase over the classifier without the screening and almost {\tt 13\%} over the {\tt sklean} baseline). 

The original set of goals included attempting to classify the readmission diagnosis as well as outperforming the {\tt sklearn} module. Both goals have been achieved, but this model presents an end accuracy which would be too low for a medical facility to use {\em in place of} a traditional medical evaluation. Rather, this project could serve as a quick indication of whether or not further testing is necessary. The majority of the transactions are technically classified correctly, and once the decision tree has been trained it only takes approximately {\tt 1 minute 22 seconds} to generate histograms and provide a diagnosis for over {\tt 30,000} transactions. 

This in turn would translate to the fact that preliminary diagnosis for {\tt 30,000} patients can be done at a high speed (Effectively making the preliminary diagnosis take approximately {\tt 2.73 {\em milliseconds per patient}}). At the very least, this would allow for patients in need of further care to be treated faster.

The training time for this decision tree is approximately {\tt 45 minutes}, but repeated training was avoided so that there was no time wasted in waiting for the same result. Instead, after the optimal threshold was determined a condition was written to prevent repeated training. If upon runtime an existing trained tree was found in the subdirectory, it was pulled and de-serialized for use. If no tree was present, then the decision tree classifier was trained and then saved by serializing the object through a {\sc pickling} method so that the tree could be called as a persistent item in the next iteration.
\item {\bf Construction Review}\newline
{\tt Note}: Histograms and Images of results are present in Section \ref{results}.\newline

Much testing has been done on this model to make it as optimal as possible in terms of delivering results. Several hundred values were tested for the preprocessing threshold, but it was determined at the end that because of the unique way in which the {\tt minithresh} was calculated inside the preprocessing routine, a threshold value of {\tt 5} is best for the cleaning and preparation stage. While this took care of the majority of the data, it came to light after building the decision tree that a higher screening bound was necessary since certain attributes had passed the preprocessing criteria but were actually irrelevant to the decision at the end. 

Attributes like `Admission ID', `Discharge ID', `Number of Outpatient/Inpatient' were essentially different for each patient (transaction). This in turn meant that the only place where these variables would be useful would be when accessing a medical database in search of a specific event. Therefore these variables could also be purged from the testing data. Here multiple combinations were tried to prevent purging of an attribute with high variance which actually had a significant impact on the end diagnosis. It was determined that {\tt 5} specific variables weren't relevant, and their removal allowed for a `boosted' accuracy.

Initially, the hypothesis made was that there were attributes such as `weight' and `insulin' which would have a big impact on the diagnosis. The hypothesis turned out to be correct. In fact, the four attributes which were found to be most significant in this study were {\sc Race}, {\sc Gender}, {\sc Age}, and {\sc Weight} in that order. Whilst attributes like {\sc Insulin} did indeed have an impact, they weren't as big in comparison.

\end{enumerate}

\subsection{Deployment}
\label{phaseSix}
\begin{enumerate}
\item {\bf Deployment Procedure}\newline
Without a standard system and format for hospitals to store medical records for patients as transactions, it is difficult to plan out the integration of this project into medical facilities. However, with the assumption that a database exists and protocols are met to preserve the anonymity of patients during testing, the entire project would need be run only once in order to train the classifier. After that a doctor would theoretically be able to send the medical data of a single patient to the tree in a {\tt list} format where each element of the {\tt list} is made anonymous and represented by a numerical value (Which the preprocessing routine will take care of). The final diagnosis will be returned in the form of an integer {\tt 0}, {\tt 1}, or {\tt 2} corresponding to one of the three diagnoses. If a value is not returned or if the value returned is not in line with the doctors' initial diagnosis, then further testing can be conducted. 

When running the project's main routine, there are various subfolders into which the histograms, the decision tree (in both pickled and text form) generated will be saved. The tree is also saved in a {\tt JavaScript} file because a small website was also written so that the tree could be rendered in a browser and traversed to see what attributes are more important. 

{\tt Note}: Due to the sheer depth and volume of this tree, most browsers will become really slow when rendering this object. Proceed Slowly. 
\item {\bf Report Generation}\newline
This report was generated alongside the development phase. After outlining the goals, the initial portion of the report along with the data description and hypotheses were discussed in detail. 
\end{enumerate}

\newpage
% ---------------------------------------------------------------------------------------------------
%                                               RESULTS
% ---------------------------------------------------------------------------------------------------
\section{Results}
\label{results}
\subsection{Decision Tree Classifier}
\label{dtc}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.9\textwidth]{TreeSample.png}
    	\caption{Partial Screen-shot of Trained Tree}
    	\label{treesample}
    \end{center}
\end{figure}
\FloatBarrier
\newpage
\subsection{Baseline Test Results}
\label{btr}
\begin{table}[htb]
\centering
\resizebox{\textwidth}{!}{%
\begin{tabular}{lllll}
{\bf Classifier} & {\bf Accuracy} & {\bf Avg Sq Error} & {\bf Mean} & {\bf Std Dev} \\
{\sc Decision Tree Classifier} & {\tt 47.31\%} & {\tt 0.83259} & {\tt 0.47556} & {\tt 0.01001} \\
{\sc Random Forest Classifier} & {\tt 55.84\%} & {\tt 0.72882} & {\tt 0.55903} & {\tt 0.00295} \\
{\sc Linear Regression} & {\tt 06.89\%} & {\tt 0.43650} & {\tt 0.06788} & {\tt 0.00919} \\
{\sc Logistic Regression} & {\tt 56.74\%} & {\tt 0.67831} & {\tt 0.56835} & {\tt 0.00533} \\
{\sc Decision Tree Regressor} & {\tt -83.41\%} & {\tt 0.85986} & {\tt -0.8458} & {\tt 0.05621} \\
{\sc AdaBoost Classifier} & {\tt 58.43\%} & {\tt 0.62442} & {\tt 0.58216} & {\tt 0.00408}
\end{tabular}%
}
\caption{Baseline Test Results}
\label{btl}
\end{table}

\FloatBarrier

\subsection{Final Results}
\label{fr}
 - The Decision Tree Classifier (Prior to High Screening) achieved an accuracy of {\tt 57.25\%}
 
\noindent - The Decision Tree Classifier (After High Screening) achieved an accuracy of {\tt 60.20\%}

\subsection{Python Modules}
\label{pymod}
This project was developed using the {\sc Spyder IDE} with the dependencies installed by the {\sc Anaconda} scientific package. A full list of all modules imported are as follows:\newline
{\tt

 - io
 
 - json
 
 - math
 
 - dill
 
 - time
 
 - time
 
 - numpy
 
 - pandas
 
 - sklearn
 
 - cPickle
 
 - warnings
 
 - pydotplus
 
 - matplotlib
 
 - collections
 
 - script.stats
}

\newpage
\subsection{Histograms}
\label{hist}
{\bf Histograms for Phase Two Attributes (In Alphabetical Order):}\newline
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{A1Cresult.png}
    	\caption{A1Cresult}
    	\label{A1Cresult}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Acarbose.png}
    	\caption{Acarbose}
    	\label{Acarbose}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Admission_Type_Id.png}
    	\caption{Admission Type ID}
    	\label{Admission Type ID}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Age.png}
    	\caption{Age}
    	\label{Age}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Chlorpropamide.png}
    	\caption{Chlorpropamide}
    	\label{Chlorpropamide}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Diag_1.png}
    	\caption{Diag 1}
    	\label{Diag 1}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Diag_2.png}
    	\caption{Diag 2}
    	\label{Diag 2}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Diag_3.png}
    	\caption{Diag 3}
    	\label{Diag 3}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Discharge_Disposition_Id.png}
    	\caption{Discharge Disposition ID}
    	\label{Discharge Disposition ID}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Gender.png}
    	\caption{Gender}
    	\label{Gender}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Glimepiride.png}
    	\caption{Glimepiride}
    	\label{Glimepiride}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Glipizide.png}
    	\caption{Glipizide}
    	\label{Glipizide}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Glyburide-Metformin.png}
    	\caption{Glyburide-Metformin}
    	\label{Glyburide-Metformin}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Glyburide.png}
    	\caption{Glyburide}
    	\label{Glyburide}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Insulin.png}
    	\caption{Insulin}
    	\label{Insulin}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Max_Glu_Serum.png}
    	\caption{Max Glu Serum}
    	\label{Max Glu Serum}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Medical_Specialty.png}
    	\caption{Medical Specialty}
    	\label{Medical Specialty}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Metformin.png}
    	\caption{Metformin}
    	\label{Metformin}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Miglitol.png}
    	\caption{Miglitol}
    	\label{Miglitol}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Nateglinide.png}
    	\caption{Nateglinide}
    	\label{Nateglinide}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Num_Lab_Procedures.png}
    	\caption{Num Lab Procedures}
    	\label{Num Lab Procedures}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Num_Medications.png}
    	\caption{Num Medications}
    	\label{Num Medications}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Num_Procedures.png}
    	\caption{Num Procedures}
    	\label{Num Procedures}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Number_Diagnoses.png}
    	\caption{Number Diagnoses}
    	\label{Number Diagnoses}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Number_Emergency.png}
    	\caption{Number Emergency}
    	\label{Number Emergency}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Number_Inpatient.png}
    	\caption{Number Inpatient}
    	\label{Number Inpatient}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Number_Outpatient.png}
    	\caption{Number Outpatient}
    	\label{Number Outpatient}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Pioglitazone.png}
    	\caption{Pioglitazone}
    	\label{Pioglitazone}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Race.png}
    	\caption{Race}
    	\label{Race}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Readmitted.png}
    	\caption{Readmitted}
    	\label{Readmitted}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Repaglinide.png}
    	\caption{Repaglinide}
    	\label{Repaglinide}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Rosiglitazone.png}
    	\caption{Rosiglitazone}
    	\label{Rosiglitazone}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Time_In_Hospital.png}
    	\caption{Time In Hospital}
    	\label{Time In Hospital}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Tolazamide.png}
    	\caption{Tolazamide}
    	\label{Tolazamide}
    \end{center}
\end{figure}
\begin{figure}[htb]
	\begin{center}
	    \includegraphics[width=0.6\textwidth]{Weight.png}
    	\caption{Weight}
    	\label{Weight}
    \end{center}
\end{figure}
\FloatBarrier
\newpage
% ---------------------------------------------------------------------------------------------------
%                                               REFERENCES
% ---------------------------------------------------------------------------------------------------
\bibliographystyle{alpha}
\bibliography{references}

% ---------------------------------------------------------------------------------------------------
%                            Tushar Iyer & Aziel Shaw | CSCI 720 | Spring 2018
% ---------------------------------------------------------------------------------------------------
 
\end{document}

