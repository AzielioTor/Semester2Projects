#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  3 14:04:48 2018

@author: Aziel Shaw
"""


class decision_tree_classifier(object):
    def __init__(self):
        return
    
    
    def fit():
        return
    
    def score():
        return
    
    def information_gain(attribute_list, full_data, picked_attribute, target_attribute):
        frequencies = {}
        subsetEntropy = 0.0
        i = attribute_list.index(picked_attribute)
    
        for entry in full_data:
            if (frequencies.has_key(entry[i])):
                frequencies[entry[i]] += 1.0
            else:
                frequencies[entry[i]]  = 1.0
    
        for val in frequencies.keys():
            valProb = frequencies[val] / sum(frequencies.values())
            dataSubset = [entry for entry in full_data if entry[i] == val]
            subsetEntropy += valProb * entropy(attribute_list, dataSubset, target_attribute)
    
        return (entropy(attribute_list, full_data, target_attribute) - subsetEntropy)
    
    
    
    def choose_attribute(full_data, attribute_list, target_attribute):
        """
        chooses the attribute among the remaining attributes which has the 
        maximum information gain. You can call information gain by just typing:
            information_gain
        """
        max_info_gain = 0
        for picked in attribute_list:
            max_info_gain = max(information_gain(attribute_list, full_data, picked, target_attribute), max_info_gain)
        for attribute in attribute_list:
            if information_gain(attribute_list, full_data, attribute, target_attribute) == max_info_gain:
                return attribute
        return 'attribute not found'
    
    def get_values(full_data, attribute_list, curr_attr):
        """
        get unique values for that particular attribute from the given data
        """
        values = []
        att_col = fulldata[curr_attr]
        for a in att_col:
            if a not in values:
                values.append(a)
        return values
    
    def get_data(full_data, attribute_list, best_pick, value):
        """
        get all the rows of the data where the chosen “best_pick” attribute has a value “value”
        """
        if best_pick not in attribute_list:
            return
        rows =[]
        for row in full_data:
            if row[best_pick] == value:
                rows.append(row)
        return rows



def main():
    return

main()

