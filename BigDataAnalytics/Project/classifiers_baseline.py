#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Aziel Shaw - 309000893
30/03/2018
Big Data Analytics Project
"""


# Imports
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing as pp
from sklearn import linear_model
from sklearn.metrics import roc_auc_score
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeRegressor
from sklearn.tree import DecisionTreeClassifier
from sklearn.cross_validation import  cross_val_score
from sklearn import metrics

##### PREPROCESSING #####
def preprocessing(data):
#   Cast the _age_ to `float`
    age = data['age']
    x = np.array(age)
    age = x.astype(float)
    data['age'] = age
#   Cast the _hours-per-week_ to `float`
    hour = data['hours-per-week']
    x = np.array(hour)
    hour = x.astype(float)
    data['hours-per-week'] = hour
#   Remove any missing values from the dataframe
    data.dropna()
#    data = consolidate_data(data)
#   Discretize the `age` variable with `cut()`
    num_bins = 20
    age_bins = pd.cut(data['age'], num_bins)
    data['age-bins'] = age_bins
#   Create bins for `hours-per-week` using `cut()`
    hours_bins = pd.cut(data['hours-per-week'], num_bins)
    data['hours_bins'] = hours_bins
#   Cross the numerical features `age` and `hours-per-week` into `age-hours`
    ages = data['age'].tolist()
    hours = data['hours-per-week'].tolist()
    agehours = []
    for i in range (0, len(ages)):
        curr_age = ages[i]
        curr_hor = hours[i]
        agehours.append(curr_age * curr_hor) 
    data["AgeAndHours"] = agehours

    age_hours_bins = pd.cut(data["AgeAndHours"], num_bins)
    data["AgeAndHours-bins"] = age_hours_bins

    le = pp.LabelEncoder()
    data = data.apply(le.fit_transform)

    y_axis = pd.DataFrame(data['income'])
    
    columns_to_drop = ['eduaction', 'income', 'hours_bins', 'age-bins', 
                       'AgeAndHours-bins', 'AgeAndHours']
    x_axis = pd.DataFrame(data).drop(columns_to_drop, axis=1)
    return x_axis, y_axis


##### Decision Tree Classifier #####
def Decision_Tree_Classifier(train_x, train_y, test_x,  test_y):
    # Load module
    clf = DecisionTreeClassifier(random_state=0)
    # Fit data
    clf.fit(train_x, train_y)
    # Get Accuracy
    accuracy = clf.score(test_x, test_y)
    # Mean Square Error
    predicted = clf.predict(test_x)
    mse = mean_squared_error(test_y, predicted)
    # Mean
    # Std Dev
    scores = cross_val_score(clf, test_x, test_y.values.ravel(), cv=10)
    return (accuracy, mse, scores.mean(), scores.std())



##### Random Forest Classifier #####
def Random_Forest_Classifier(train_x, train_y, test_x,  test_y, max_depth_v):
    # Load Module
    clf = RandomForestClassifier(max_depth=max_depth_v, random_state=0)
    # Fit Data
    clf.fit(train_x, train_y.values.ravel())
    # Get Accuracy
    accuracy = clf.score(test_x, test_y)
    # Mean Square Error
    predicted = clf.predict(test_x)
    mse = mean_squared_error(test_y, predicted)
    # Mean
    # Std Dev
    scores = cross_val_score(clf, test_x, test_y.values.ravel(), cv=10)
    return (accuracy, mse, scores.mean(), scores.std())



##### Linear_Regression #####
def Linear_Regression(train_x, train_y, test_x,  test_y):
#     - Linear Regression
    lin_reg = linear_model.LinearRegression()
    lin_reg.fit(train_x, train_y)
#   Get rsquared
    accuracy = lin_reg.score(test_x, test_y)
    # Mean Square Error
    predicted = lin_reg.predict(test_x)
    mse = mean_squared_error(test_y, predicted)
    # Mean
    # Std Dev
    scores = cross_val_score(lin_reg, test_x, test_y.values.ravel(), cv=10)
    return (accuracy, mse, scores.mean(), scores.std())


##### Logistic_Regression #####
def Logistic_Regression(train_x, train_y, test_x,  test_y):
#     - Logistic Regression
    log_reg = linear_model.LogisticRegression()
#   Fit model to training data
    log_reg.fit(train_x, train_y)
#   Get Accuracy for logistic regression
    accuracy = log_reg.score(test_x, test_y)
    # Mean Square Error
    predicted = log_reg.predict(test_x)
    mse = mean_squared_error(test_y, predicted)
    # Mean
    # Std Dev
    scores = cross_val_score(log_reg, test_x, test_y.values.ravel(), cv=10)
    return (accuracy, mse, scores.mean(), scores.std())


##### Decision Tree Regressor #####
def Decision_Tree_Regressor(train_x, train_y, test_x,  test_y):
    # Load module
    clf = DecisionTreeRegressor(random_state=1)
    # Fit data
    clf.fit(train_x, train_y)
    # Get Accuracy
    accuracy = clf.score(test_x, test_y)
    # Mean Square Error
    predicted = clf.predict(test_x)
    mse = mean_squared_error(test_y, predicted)
    # Mean
    # Std Dev
    scores = cross_val_score(clf, test_x, test_y.values.ravel(), cv=10)
    return (accuracy, mse, scores.mean(), scores.std())



##### Adaboost Classifier #####
def AdaBoost_Classifier(train_x, train_y, test_x,  test_y):
    # Load module
    clf = AdaBoostClassifier()
    # Fit data
    clf.fit(train_x, train_y)
    # Get Accuracy
    accuracy = clf.score(test_x, test_y)
    # Mean Square Error
    predicted = clf.predict(test_x)
    mse = mean_squared_error(test_y, predicted)
    # Mean
    # Std Dev
    scores = cross_val_score(clf, test_x, test_y.values.ravel(), cv=10)
    return (accuracy, mse, scores.mean(), scores.std())



##########################
##### HELPER METHODS #####


def print_table(classifiers):
    table =  "/*********************************************************************************************\\\n"
    table += "|      Classifier      |   Accuracy    |  Mean Square Error  |      Mean      |    Std Dev    |\n"
    table += "|---------------------------------------------------------------------------------------------|\n"
    for classifier in classifiers:
        name = classifier[1]
        data = classifier[0]
        
        table += "| " + name + " | "
        table += str(data[0])[:13] + " | "
        table += "   " + str(data[1])[:13] + "    | "
        table += " " + str(data[2])[:13] + " |"
        table += " " + str(data[3])[:13] + " |"
        table += "\n"
        
    table += "\\*********************************************************************************************/\n"
    print(table)
    return


def get_baseline(train_x, train_y, test_x,  test_y):
    #### START ANALYSIS####
    classifiers = []
    # Decision Tree Classifier
    dec_tre_acc = Decision_Tree_Classifier(train_x, train_y, test_x,  test_y)
    classifiers.append((dec_tre_acc, "Dcision Tre Clasfier"))
    # Random Forest Classifier   NOTE THIS TAKES EXTRA VARIABLE max_depth 
    ran_for_acc = Random_Forest_Classifier(train_x, train_y, test_x,  test_y, 3)
    classifiers.append((ran_for_acc, "Rndom Forst Clasfier"))
    # Linear Regression
    lin_reg_acc = Linear_Regression(train_x, train_y, test_x,  test_y)
    classifiers.append((lin_reg_acc, "Linear Regression   "))
    # Logistic Regression
    log_reg_acc = Logistic_Regression(train_x, train_y, test_x,  test_y)
    classifiers.append((log_reg_acc, "Logistic Regression "))
    # Decision Tree Regressor
    tre_reg_acc = Decision_Tree_Regressor(train_x, train_y, test_x,  test_y)
    classifiers.append((tre_reg_acc, "Dcision Tre Regresor"))
    # Adaboost Classifier
    ada_boo_acc = AdaBoost_Classifier(train_x, train_y, test_x,  test_y)
    classifiers.append((ada_boo_acc, "Adaboost Classifier "))
    
    ##### POPULATE TABLE #####
    print_table(classifiers)
    return


##### MAIN METHOD #####
#def main():
#    # Read in data and add columns
#    training = pd.read_csv("adult.data.txt", header=None)
#    testing  = pd.read_csv("adult.test.txt", header=None)
#    column_headers = ['age', 'workclass', 'fnlwgt', 'eduaction', 
#                      'eduaction-num', 'marital-status', 'occupation', 
#                      'relationship', 'race', 'sex', 'capital-gain', 
#                      'capital-loss', 'hours-per-week', 'native-country', 
#                      'income']
#    training.columns = column_headers
#    testing.columns  = column_headers
#    # preprocessing
#    train_x, train_y = preprocessing(training)
#    test_x,  test_y  = preprocessing(testing)
#    #### START ANALYSIS####
#    classifiers = []
#    # Decision Tree Classifier
#    dec_tre_acc = Decision_Tree_Classifier(train_x, train_y, test_x,  test_y)
#    classifiers.append((dec_tre_acc, "Dcision Tre Clasfier"))
#    # Random Forest Classifier   NOTE THIS TAKES EXTRA VARIABLE max_depth 
#    ran_for_acc = Random_Forest_Classifier(train_x, train_y, test_x,  test_y, 3)
#    classifiers.append((ran_for_acc, "Rndom Forst Clasfier"))
#    # Linear Regression
#    lin_reg_acc = Linear_Regression(train_x, train_y, test_x,  test_y)
#    classifiers.append((lin_reg_acc, "Linear Regression   "))
#    # Logistic Regression
#    log_reg_acc = Logistic_Regression(train_x, train_y, test_x,  test_y)
#    classifiers.append((log_reg_acc, "Logistic Regression "))
#    # Decision Tree Regressor
#    tre_reg_acc = Decision_Tree_Regressor(train_x, train_y, test_x,  test_y)
#    classifiers.append((tre_reg_acc, "Dcision Tre Regresor"))
#    # Adaboost Classifier
#    ada_boo_acc = AdaBoost_Classifier(train_x, train_y, test_x,  test_y)
#    classifiers.append((ada_boo_acc, "Adaboost Classifier "))
#    
#    ##### POPULATE TABLE #####
#    print_table(classifiers)
#    
#    return (train_x, train_y, test_x,  test_y)
#
#a, b, c, d = main()
