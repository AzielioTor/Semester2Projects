#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 23 11:51:08 2018

@author: tushariyer
"""
import warnings
import numpy as np
from matplotlib import mlab
from scipy.stats import norm
from matplotlib import colors
from matplotlib import pyplot as plt
from sklearn.preprocessing import LabelEncoder as LE

warnings.filterwarnings("ignore")


# ------------------------------
#        PREPARE DATA
# ------------------------------
def prune_data(df, threshold):
    # Columsn to prevent pruning
    protected_columns = ['readmitted', 'race', 'gender', 'age', 'time_in_hospital', 'num_lab_procedures', 'num_procedures', 'num_medications', 'A1Cresult', 'insulin']
    print "\nEstablished Protected Variables"
    
    mini_thresh = int(np.floor(threshold / 2))  # Lower threshold for sub-operations
    
    df = df.dropna()  # Get ride of missing data
    print "\nMissing Data Removed"
    
    column_list = check_unique(df, mini_thresh, protected_columns)  # Check for the number of unique variables
    print "\nPrimary Pruning Complete"
    
    # Along with all variables that aren't so unique, the following are also useless
    column_list.extend(('payer_code', 'encounter_id', 'patient_nbr', 'admission_source_id')) 
    
    # In the event a column has the same value throughout, it won't affect the classifier.
    df = df.drop(column_list, axis=1)  # Drop useless attributes
    df = numericize(df)  # Convert from categorical representation
    
    second_pruning = []  # Store attributes for secondary pruning here
    
    print "\nComputing Histograms"
    for column_name, column in df.transpose().iterrows():
        
        # Clean up parameters
        clean_title = column_name.replace("_", " ")
        labelx = "Bins for " + column_name
        
        # Compute histograms
        hist_data = plot_histogram(column, clean_title, labelx, "Frequency")
        
        # If the number of bars is below the threshold, and if the attribute is not protected
        if (len(hist_data[1]) <= threshold) and (column_name not in protected_columns):
                second_pruning.append(column_name)  # Add it to the list
    
    print "\nNOTE: Histogram values have been normalized"
    
    df = df.drop(second_pruning, axis=1)  # Drop unprotected attributes below threshold
    print "\nSecondary Pruning Complete"
    
    return df



# ------------------------------
#        CHECK UNIQUENESS
# ------------------------------
def check_unique(df, threshold, protected_columns):
    column_list = []  # List of attributes to die
    
    # Check unqiueness of the attributes
    for column_name, column in df.transpose().iterrows():
        uniqueness = count_unique(column)  # Retrieve uniqueness of a column
        
        # If below threshold and not protected
        if (uniqueness <= threshold) and (column_name not in protected_columns):
            column_list.append(column_name)  # Add to death list
            
    return column_list



# ------------------------------
#        COUNT UNIQUENESS
# ------------------------------
def count_unique(column):
    return column.nunique()  # Return number of unique values



# ------------------------------
#        SHOW UNIQUENESS
# ------------------------------
def show_unique(column_name, column):
    # Format string for printing first
    col = column_name + ": "
    col += str(column.nunique())
    print col, "Unique Values"
    print column.unique()  # Print unique values
    print "\n"



# ------------------------------
#         PLOT HISTOGRAM
# ------------------------------
def plot_histogram(feature, title, labelx, labely):
    title = title.title()  # Uppercase the first letter
    
    # Let's make this a cool looking histogram
    bg_color = 'black'
    fg_color = 'turquoise'
    
    # White on black
    fig = plt.figure(facecolor=bg_color, edgecolor=fg_color)
    axes = fig.add_subplot(111)
    axes.patch.set_facecolor(bg_color)
    axes.xaxis.set_tick_params(color=fg_color, labelcolor=fg_color)
    axes.yaxis.set_tick_params(color=fg_color, labelcolor=fg_color)
    
    # Plot border
    for spine in axes.spines.values():
        spine.set_color(fg_color)
    
    # Custom bin data
    mins = min(feature)
    maxs = max(feature)
    if mins > maxs:
        mins, maxs = maxs, mins
        
    bin_vals = np.arange(mins, maxs + 1.5, 1)  # Custom bin values
    
    # Create histogram and pull data
    n, nbins, patches = plt.hist(feature, bins=bin_vals, linewidth=1.2, rwidth=1.2, normed=True)
    
    # Change bar colors depending on height
    fracs = n / n.max()
    color_norm = colors.Normalize(fracs.min(), fracs.max())
    
    # Set various colors
    for thisfrac, thispatch in zip(fracs, patches):
        color = plt.cm.plasma(color_norm(thisfrac))
        thispatch.set_facecolor(color)
    
    (mu, sigma) = norm.fit(feature)  # Statistics
    y = mlab.normpdf(nbins, mu, sigma) * sum(n * np.diff(nbins))
    
    # Add title and labels
    plt.title(r'$\mathrm{Histogram\ of\ %s:}\ \mu=%.3f,\ \sigma=%.3f$' %(title, mu, sigma), color=fg_color)
    plt.xlabel(labelx, color=fg_color)
    plt.autoscale(enable=True, axis='y', tight=False)
    plt.ylabel(labely, color=fg_color)
    plt.tight_layout()
    plt.plot(nbins, y, '--', linewidth=1.3, color='cornflowerblue')
    
    # Print out the values for histograms that have at most 10 bars
    if len(nbins) <= 10:
        for i in range(len(nbins) - 1):
            plt.text(nbins[i],n[i],str(format(n[i], '.5f')), weight='heavy', color=fg_color)
    
    plt.show()
    
    return n, nbins, patches  # Return data for categorization



# ------------------------------
#          NUMERICIZE
# ------------------------------
def numericize(df):
    df = df.apply(LE().fit_transform)
    print "\nData converted from categorical to numerical representation"
    
    return df


