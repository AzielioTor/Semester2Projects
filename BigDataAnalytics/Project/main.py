#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 23 11:52:15 2018

@author: tushariyer
"""
import warnings
import pandas as pd
from preprocessing import prune_data as prune
from adaboost_classifier import get_error_rate
from adaboost_classifier import plot_error_rate
from sklearn.tree import DecisionTreeClassifier
from adaboost_classifier import adaboost_classifier
from classifier_baseline import get_baseline as base
from sklearn.model_selection import train_test_split as tts

warnings.filterwarnings("ignore")


# ------------------------------
#              MAIN
# ------------------------------
def main():
    df_original = pd.read_csv("diabetic_data.csv")  # Import data
    
    threshold = 5  # Toggle for severity of attribute pruning. Does not affect protected attributes
    print "\nThreshold set at:",threshold
    
    print "\nBeginning Preprocessing"
    df_preprocessed = prune(df_original, threshold)
    print "\nPreprocessing Complete"
    
    print "\nOriginal Dimensions:", df_original.shape
    print "Preprocessed Dimensions:", df_preprocessed.shape
    
    # Prep the data for the baseline tests
    data = final_prep(df_preprocessed)
    
    # Get baseline
    print "\nGetting baseline calculations\n"
    base(data[0], data[1], data[2], data[3])
    
    # Prep Adaboost
    print "\nPreparing for AdaBoost"
    prep_boosting(data[0], data[1], data[2], data[3])


# ------------------------------
#       PREP FOR BASELINE
# ------------------------------
def final_prep(df):
    # Split the target attribute into its own dataframe
    target = ['readmitted']
    df_y = df['readmitted']
    df_x = df.drop(target, axis=1)
    
    # Split into training and testing data
    train_x, test_x, train_y, test_y = tts(df_x, df_y, test_size=0.33, random_state=42)  
    
    print "\nData Spliting Complete"
    
    return [train_x, train_y, test_x, test_y]


# ------------------------------
# GENERIC CLASSIFIER ERROR RATES
# ------------------------------
def dtree_error_rates(train_x, train_y, test_x, test_y):
    dtree = DecisionTreeClassifier(random_state = 1, max_depth = 1)  # Load classifier
    dtree.fit(train_x, train_y)  # Fit model
    
    pred_train = dtree.predict(train_x)  # Get train preditions
    pred_test = dtree.predict(test_x)  # Get test predictions
    
    return get_error_rate(pred_train, train_y), get_error_rate(pred_test, test_y), dtree  # Return error rates


# ------------------------------
#      PREP FOR ADABOOST
# ------------------------------
def prep_boosting(train_x, train_y, test_x, test_y):
    print "\nPreparing for AdaBoost"
    
    print "\nFitting a simple decision tree"
    results = dtree_error_rates(train_x, train_y, test_x, test_y)  # Fit a simple decision tree
    
    print "\nGetting Error Results"
    generic_train, generic_test, dtree = results[0], results[1], results[2]  # Split results
    
    generic_train = [generic_train]
    generic_test = [generic_test]
    
    print "\nComputing AdaBoost for a range of M values from 0 to 210 [Step: 5]"
    data_range = range(0, 5, 1)  # Iterations to test
#    scores = []
#    preds = []
    for step in data_range:    
        print " - Computing AdaBoost when M = ", step
        iterations = adaboost_classifier(train_x, train_y, test_x, test_y, step, dtree)
        generic_train.append(iterations[0])
        generic_test.append(iterations[1])
#        preds = iterations[2]
#        scores.append(iterations[1])
    
    # Compare error rate vs number of iterations
    print "\nPlotting Error rates"
    plot_error_rate(generic_train, generic_test)
    
#    best_score = max(scores)
#    m_index = 210
#    print "\n\nBest AdaBoost Score achieved:", scores[-1]
#    print "M value for best score:", m_index
    # Print final adaboost accuracy
#    print preds



# ------------------------------
#              INIT
# ------------------------------
if __name__ == "__main__":
    main()  # Run main routine