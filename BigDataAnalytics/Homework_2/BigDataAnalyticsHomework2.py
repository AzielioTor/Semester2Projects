"""
Big Data Analytics - Homework 2
Aziel Shaw - 309000893
02/05/2018
"""


import pandas
from sklearn import preprocessing as pp
from sklearn.decomposition import PCA
from matplotlib import pyplot
import numpy


print("\n Question 1 ")
# Read in dataset and remove unneeded columns
places_rated_data = pandas.read_csv("places_rated_new.data")
places_rated_modded = places_rated_data.drop(['City', 'CaseNum', 'Long', 'Lat',
                                              'Pop', 'StNum'], axis=1)
cities = places_rated_data.drop(['Climate', 'HousingCost', 'HlthCare', 
                                     'Transp', 'Educ', 'Arts', 'Recreat', 
                                     'Econ', 'Crime', 'CaseNum', 'Long', 'Lat', 
                                     'Pop', 'StNum'], axis=1).values
# Normalize Dataset
normalizedX = pp.scale(places_rated_modded)

# PCA
pca = PCA(n_components=None, copy=True, whiten=False, svd_solver='auto', 
          tol=0.0, iterated_power='auto', random_state=None)
pca = pca.fit(normalizedX)

print("\n\nExplained Variance Ratio:")  
print(pca.explained_variance_ratio_)  

# Ger variables from the PCA
exp_var = pca.explained_variance_
exp_var_rat = pca.explained_variance_ratio_
pca_comp = pca.components_.T
load_vec = (numpy.sqrt(exp_var) * pca_comp)

# Show data on plot.
plot_data = numpy.cumsum(exp_var_rat)
pyplot.plot(plot_data)
pyplot.gca()
pyplot.show(); # This plot shows that 4 PCs are needed for 80%
print("\n\n\n\n\n\n Question 2")

PCA_1 = load_vec[:,0]
PCA_2 = load_vec[:,1]
PCA_3 = load_vec[:,2]
# print loading vector information
print("\nPCA_1")
print(PCA_1)
print("\nPCA_2")
print(PCA_2)
print("\nPCA_3")
print(PCA_3)

print("\n\n\n\n\n\n Question 3 & 4")
pca = PCA(n_components=2).fit(normalizedX)
transformed_data = pca.transform(normalizedX)
transformed_data = pandas.DataFrame(transformed_data, columns = ['Climate', 'Housing Cost'])
three_components = pandas.DataFrame({'PCA_1': PCA_1, 'PCA_2': PCA_2, 'PCA_3': PCA_3}, columns=['PCA_1', 'PCA_2', 'PCA_3'])
figure, axis = pyplot.subplots(figsize = (14,8))
plot_x = transformed_data.loc[:, 'Climate']
plot_y = transformed_data.loc[:, 'Housing Cost']
# Create plot with vectors and datapoitns
axis.scatter(plot_x, plot_y, facecolors='r', edgecolors='r', s=20)
vectors = pca.components_.T
arrow_config, text_position = 7.0, 8.0,
# Name the different data points with names of the cities
for i, data in enumerate(cities):
        axis.annotate(data, (plot_x[i],plot_y[i]))
# Draw the PCA Plot
for i, vector in enumerate(vectors):
    # Draw the arrow
    axis.arrow(0, 0, arrow_config*vector[0], arrow_config*vector[1], color='green', head_width=0.2, head_length=0.2, linewidth=2)
    # Write text by the arrows
    axis.text(vector[0]*text_position, vector[1]*text_position, three_components.columns[i], ha='center', va='center', color='blue', fontsize=16)	

    
