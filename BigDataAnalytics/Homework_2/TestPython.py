"""
Big Data Analytics - Homework 2
Aziel Shaw - 309000893
02/05/2018
"""


import pandas
from sklearn import preprocessing as pp
from sklearn.decomposition import PCA
from matplotlib import pyplot
import numpy


print("\n Question 1 \n\n")
# Read in dataset and remove unneeded columns
places_rated_data = pandas.read_csv("places_rated_new.data")
places_rated_modded = places_rated_data.drop(['City', 'CaseNum', 'Long', 'Lat',
                                              'Pop', 'StNum'], axis=1)
# Normalize Dataset
normalizedX = pp.scale(places_rated_modded)
print(normalizedX)

# PCA
pca = PCA(n_components=None, copy=True, whiten=False, svd_solver='auto', 
          tol=0.0, iterated_power='auto', random_state=None)
pca = pca.fit(normalizedX)
#pca = pca.fit_transform(normalizedX)
print("\n\nExplained Variance Ratio:")  
print(pca.explained_variance_ratio_)  

exp_var = pca.explained_variance_
exp_var_rat = pca.explained_variance_ratio_
mean = pca.mean_
sin_val = pca.singular_values_
noise_var = pca.noise_variance_
pca_comp = pca.components_.T
load_mat = (numpy.sqrt(exp_var) * pca_comp)
#loading_vectors = pca.


plot_data = numpy.cumsum(exp_var_rat)
pyplot.plot(plot_data)
pyplot.gca()
pyplot.show() # This plot shows that 4 PCs are needed for 80%
print("\n\n\n\n\n\n Question 2")

PCA_1_load_mat = load_mat[:,0]
PCA_2_load_mat = load_mat[:,1]
PCA_3_load_mat = load_mat[:,2]

#PCA_1 = load_vectors
PCA_2 = PCA_2_load_mat
PCA_3 = PCA_3_load_mat


print("\n\nPCA 1:\n")
print(PCA_1)
print("\n\nPCA 2:\n")
print(PCA_2)
print("\n\nPCA 3:\n")
print(PCA_3)

print("\n\n\n\n\n\n Question 3 & 4")
pca_fit_trans = pca.fit_transform(normalizedX)
test_array = [PCA_1, PCA_2]
print(test_array)

#pca_fit_trans = pca.fit_transform(test_array)



#print("First shot")
#mval =  mean[-2:]
#def draw_vector(v0, v1, ax=None):
#    ax = ax or pyplot.gca()
#    arrowprops=dict(arrowstyle='->',
#                    linewidth=2,
#                    shrinkA=0, shrinkB=0)
#    ax.annotate('', v1, v0, arrowprops=arrowprops)
#
## plot data
#pyplot.scatter(pca_fit_trans[:, 0], pca_fit_trans[:, 1], alpha=0.2)
#for length, vector in zip(pca.explained_variance_, pca.components_[0]):
#    v = vector * 3 * numpy.sqrt(length)
#    draw_vector(mval, (mval + v))
#pyplot.xlabel('component 1')
#pyplot.ylabel('component 2')
#pyplot.axis('equal');



print("Second shot")
mval =  mean[-2:]
def draw_vector(v0, v1, ax=None):
    ax = ax or pyplot.gca()
    arrowprops=dict(arrowstyle='->',  linewidth=2,shrinkA=0, shrinkB=0)
#    arrowprops=dict(arrowstyle='->',
#                    linewidth=2,
#                    shrinkA=0, shrinkB=0)
    ax.annotate('', v1, v0, arrowprops=arrowprops)

# plot data
pyplot.scatter(pca_fit_trans[:, 0], pca_fit_trans[:, 1], alpha=0.2)
for length, vector in zip(pca.explained_variance_, pca.components_[0]):
    v = vector * 3 * numpy.sqrt(length)
    draw_vector(mval, (mval + v))
pyplot.axis('equal');




#xvector = pca.components_[0]
#yvector = pca.components_[1]
#
#PCA1_PCA2 = ""
#
#xs = pca.transform(normalizedX)[:,0]
#ys = pca.transform(normalizedX)[:,1]
#for i in range(len(xvector)):
#    pyplot.arrow(0, 0, xvector[i]*max(xs), yvector[i]*max(ys), color='r', width=0.0005, head_width=0.0025)
#    pyplot.text(xvector[i]*max(xs)*1.2, yvector[i]*max(ys)*1.2, normalizedX, color='r')
#
#for i in range(len(xs)):
#    pyplot.plot(xs[i], ys[i], 'bo')
#    pyplot.text(xs[i]*1.2, ys[i]*1.2, normalizedX, color='b')
#
#pyplot.show()



#
## 0,1 denote PC1 and PC2; change values for other PCs
##xvector = pca.components_[0] # see 'prcomp(my_data)$rotation' in R
##yvector = pca.components_[1]
#
#xs = pca.transform(normalizedX)[:,0] # see 'prcomp(my_data)$x' in R
#ys = pca.transform(normalizedX)[:,1]
#
#exp_var = pca.explained_variance_
#pca_comp = pca.components_.T
#load_mat = (numpy.sqrt(exp_var) * pca_comp)
#
#
#PCA_1 = load_mat[:,0]
### visualize projections
#    
### Note: scale values for arrows and text are a bit inelegant as of now,
###       so feel free to play around with them
#
#for i in range(len(xvector)):
## arrows project features (ie columns from csv) as vectors onto PC axes
#    pyplot.arrow(0, 0, xvector[i]*max(xs), yvector[i]*max(ys), color='r', width=0.0005, head_width=0.0025)
#    pyplot.text(xvector[i]*max(xs)*1.2, yvector[i]*max(ys)*1.2, normalizedX[i], color='r')
#
#for i in range(len(xs)):
## circles project documents (ie rows from csv) as points onto PC axes
#    pyplot.plot(xs[i], ys[i], 'bo')
#    pyplot.text(xs[i]*1.2, ys[i]*1.2, normalizedX[i], color='b')
#
#pyplot.show()
#


