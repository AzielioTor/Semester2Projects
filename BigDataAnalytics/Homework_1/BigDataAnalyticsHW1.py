# -*- coding: utf-8 -*-
"""
Aziel Shaw - 309000893
Big Data Analytics Homework 1
29/01/2018
"""
# Imports
import pandas
import numpy
from sklearn import preprocessing as pp
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.metrics.pairwise import cosine_distances
from scipy.spatial.distance import cdist
import matplotlib.pyplot as matpp



"""
HW1 Question 3
PART A
"""
print("Starting Question 3A")
iris_data = pandas.read_table("iris.data", ",", header=None)
iris_data.columns = ['1','2','3','4','5']
iris_data_modded = iris_data.drop(['5'], axis=1)
X = iris_data_modded.as_matrix()
print("FINISHING Question 3A")
"""
HW1 Question 3
PART B
"""
print("Starting Question 3B")
normalizedX = pp.normalize(X)
print("FINISHING Question 3B")
"""
HW1 Question 3
PART C
"""
print("Starting Question 3C")
print("Question 3C I: Euclidan Distance")
euclidan_distance_X = pairwise_distances(normalizedX)
print("Question 3C II: 1-Cosine Similarity")
cosine_X = cosine_distances(normalizedX)
print("Question 3C III: Mahalanobis Distance")
mahalanobis_X = cdist(normalizedX, normalizedX, 'mahalanobis', VI=None)
print("FINISHING Question 3C")
"""
HW1 Question 3
PART D
"""
print("Starting Question 3D")
plot_color = "OrRd"
print("Generating Euclidian Plot")
euclidian_plot = matpp.matshow(euclidan_distance_X, cmap=plot_color)
euclidian_plot = matpp.gca()
euclidian_plot = matpp.title('Plot of Euclidian Distance using Normalized X', y=1.08)
print("Generating Cosine Plot")
cosine_plot = matpp.matshow(cosine_X, cmap=plot_color)
cosine_plot = matpp.gca()
cosine_plot = matpp.title('Plot of Cosine Similarity using Normalized X', y=1.08)
print("Generating Mahalanobis Plot")
mahalanobis_plot = matpp.matshow(mahalanobis_X, cmap=plot_color)
mahalanobis_plot = matpp.gca()
mahalanobis_plot = matpp.title('Plot of Mahalanobis Distance using Normalized X', y=1.08)
print("Showing Plots...")
#euclidian_plot.show()
#cosine_plot.show()
#mahalanobis_plot.show()
print("FINISHING Question 3D")
"""
HW1 Question 3
PART E
"""
print("Starting Question 3E")
print("Euclidian Distance 3E...")
# initial split of euclidian matrix...
# Split into 3 columns...
e_col_split = numpy.hsplit(euclidan_distance_X, 3)
e_l_col = e_col_split[0]
e_m_col = e_col_split[1]
e_r_col = e_col_split[2]
# Split each column into it's own variable...
e_l_col_split = numpy.vsplit(e_l_col, 3) # Left Column...
e_l_col_upper = e_l_col_split[0]
e_l_col_midle = e_l_col_split[1]
e_l_col_botom = e_l_col_split[2]
e_m_col_split = numpy.vsplit(e_m_col, 3) # Middle Column...
e_m_col_upper = e_m_col_split[0]
e_m_col_midle = e_m_col_split[1]
e_m_col_botom = e_m_col_split[2]
e_r_col_split = numpy.vsplit(e_r_col, 3) # Right Column...
e_r_col_upper = e_r_col_split[0]
e_r_col_midle = e_r_col_split[1]
e_r_col_botom = e_r_col_split[2]
# Averages results into array
e_averages_of_sections = [[numpy.tril(e_l_col_upper).mean(),           (e_m_col_upper).mean(),           (e_r_col_upper).mean()],
                          [          (e_l_col_midle).mean(), numpy.tril(e_m_col_midle).mean(),           (e_r_col_midle).mean()],
                          [          (e_l_col_botom).mean(),           (e_m_col_botom).mean(), numpy.tril(e_r_col_botom).mean()]]
# Print array
print(e_averages_of_sections[0])
print(e_averages_of_sections[1])
print(e_averages_of_sections[2])
print("Cosine Distance 3E...")
# initial split of cosine matrix...
# Split into 3 columns...
c_col_split = numpy.hsplit(cosine_X, 3)
c_l_col = c_col_split[0]
c_m_col = c_col_split[1]
c_r_col = c_col_split[2]
# Split each column into it's own variable...
c_l_col_split = numpy.vsplit(c_l_col, 3) # Left Column...
c_l_col_upper = c_l_col_split[0]
c_l_col_midle = c_l_col_split[1]
c_l_col_botom = c_l_col_split[2]
c_m_col_split = numpy.vsplit(c_m_col, 3) # Middle Column...
c_m_col_upper = c_m_col_split[0]
c_m_col_midle = c_m_col_split[1]
c_m_col_botom = c_m_col_split[2]
c_r_col_split = numpy.vsplit(c_r_col, 3) # Right Column...
c_r_col_upper = c_r_col_split[0]
c_r_col_midle = c_r_col_split[1]
c_r_col_botom = c_r_col_split[2]
# Averages results into array
c_averages_of_sections = [[numpy.tril(c_l_col_upper).mean(),           (c_m_col_upper).mean(),           (c_r_col_upper).mean()],
                          [          (c_l_col_midle).mean(), numpy.tril(c_m_col_midle).mean(),           (c_r_col_midle).mean()],
                          [          (c_l_col_botom).mean(),           (c_m_col_botom).mean(), numpy.tril(c_r_col_botom).mean()]]
# Print array
print(c_averages_of_sections[0])
print(c_averages_of_sections[1])
print(c_averages_of_sections[2])
print("Mahalanobis Distance 3E...")
# initial split of Mahalanobis matrix...
# Split into 3 columns...
m_col_split = numpy.hsplit(mahalanobis_X, 3)
m_l_col = m_col_split[0]
m_m_col = m_col_split[1]
m_r_col = m_col_split[2]
# Split each column into it's own variable...
m_l_col_split = numpy.vsplit(m_l_col, 3) # Left Column...
m_l_col_upper = m_l_col_split[0]
m_l_col_midle = m_l_col_split[1]
m_l_col_botom = m_l_col_split[2]
m_m_col_split = numpy.vsplit(m_m_col, 3) # Middle Column...
m_m_col_upper = m_m_col_split[0]
m_m_col_midle = m_m_col_split[1]
m_m_col_botom = m_m_col_split[2]
m_r_col_split = numpy.vsplit(m_r_col, 3) # Right Column...
m_r_col_upper = m_r_col_split[0]
m_r_col_midle = m_r_col_split[1]
m_r_col_botom = m_r_col_split[2]
# Averages results into array
m_averages_of_sections = [[numpy.tril(m_l_col_upper).mean(),           (m_m_col_upper).mean(),           (m_r_col_upper).mean()],
                          [          (m_l_col_midle).mean(), numpy.tril(m_m_col_midle).mean(),           (m_r_col_midle).mean()],
                          [          (m_l_col_botom).mean(),           (m_m_col_botom).mean(), numpy.tril(m_r_col_botom).mean()]]
# Print array
print(m_averages_of_sections[0])
print(m_averages_of_sections[1])
print(m_averages_of_sections[2])
print("FINISHING Question 3E")

