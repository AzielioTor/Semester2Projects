#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 14 16:34:56 2018

@author: tushariyer
"""

import pandas as pd
import numpy as np
import random
import time
from apriori import runApriori  # Given Apriori Script
from fp_growth import find_frequent_itemsets  # Given FP Growth Script
from matplotlib import pyplot as plt

minSup = [0.02, 0.05, 0.10, 0.15, 0.20, 0.50]  # This stores the different support values we will be using
transactions = []  # This is where we will store all our transactions

# Lists to store the times taken by each run through with varying minimum support
apriori_times = []
fpgrowth_times = []

# Read in places_rated.data as data frame
df_original = pd.read_csv("adult.data", header=None)

# Add header names
df_original.columns = ['age', 'workclass', 'fnlwgt', 'education', 'education-num', 'marital-status', 'occupation', 'relationship', 'race', 'sex', 'capital-gain', 'capital-loss', 'hours-per-week', 'native-country', '50k']

# Save Continuous data so we can categorize it
income = df_original['50k'].tolist()
age = df_original['age'].tolist()
capital_gain = df_original['capital-gain'].tolist()
capital_loss = df_original['capital-loss'].tolist()
hours_week = df_original['hours-per-week'].tolist()

# Drop specified features
df_dropped = (df_original.drop(['fnlwgt', 'education-num'], axis=1))


"""
Convert continuous features to categories
"""

"""
Plot the histogram of the list passed in
"""
def histogram(feature, title, label, color):
    # Create histogram and pull data
    n, nbins, patches = plt.hist(feature, facecolor=color, alpha=0.7, edgecolor='black', linewidth=1.2)
    
    # Add title and labels
    plt.title(title)
    plt.xlabel(label)
    plt.ylabel("Frequency/Amount")
    plt.show()
    
    return n, nbins, patches  # Return data for categorization


"""
Categorize a dataframe column by using the data from a histogram of the column
"""
def categorize(feature, title, label, color, column):
    print "\n\n\nCategorizing " + column + ":"
    peaks, feature_bins, patches = histogram(feature, title, label, color)
    
    # Categorize value
    feature_labels = range(1, len(feature_bins) + 1)  # Create number of categories
    feature_labels = map(str, feature_labels)  # Convert to string array
    
    # Access dataframe column and replace with categories
    df_dropped[column] = pd.cut(df_dropped[column], len(feature_bins), labels=feature_labels)


# Let's categorize these features:
    
# Age 
categorize(age, "Age Histogram", "Age", "Blue", "age")

# Capital Gains
categorize(capital_gain, "Capital Gain Histogram", "Capital Gain", "Green", "capital-gain")

# Capital Losses
categorize(capital_loss, "Capital Loss Histogram", "Capital Loss", "Red", "capital-loss")

# Hours worked per week
categorize(hours_week, "Hours Worked Histogram", "Hours Worked in a Week", "Orange", "hours-per-week")

print "\n\nPreprocessing complete.\n"

# Populate transactions 
for rows in df_dropped.itertuples():
    transactions.append(list(rows))  # Making sure that we're passing lists and not DF series objects


"""
Plot Apriori and FP Growth Vs. Min.Support for every minimum support value
"""
def create_plots(data_apriori, data_fpgrowth, support):
    
    # Variables for graph x-axis ticks. Using truncated ints to improve readability
    apriori_ticks = [int(i) for i in data_apriori]
    fpgrowth_ticks = [int(i) for i in data_fpgrowth]
    
    # Plot runtimes for Apriori
    print "\n\nPlotting Apriori Runtimes"    
    
    plt.plot(data_apriori, support, color="blue", alpha=0.5, linewidth=3.0)  # Plot line
    plt.scatter(data_apriori, support, facecolors='c', edgecolors='c', s=70)  # Plot points
    for xy in zip(apriori_ticks, support):
        plt.annotate('(%s, %s)' % xy, xy=xy, textcoords='data')  # Annotate coordinates
    
    plt.title("Runtimes for Apriori Vs. Minimum Support")  # Add title
    plt.xlabel("Apriori Runtimes (In Seconds)")  # Add x-axis label
    plt.xticks(apriori_ticks, apriori_ticks)  # Mark x-axis ticks
    plt.ylabel("Minimum Support Values")  # Add y-axis label
    
    plt.grid()  # Show grid and graph
    plt.show()
    
    # Plot runtimes for FP Growth
    print "\n\nPlotting FP Growth Runtimes"   
    
    plt.plot(data_fpgrowth, support, color="blue", alpha=0.5, linewidth=3.0)  # Plot line
    plt.scatter(data_fpgrowth, support, facecolors='c', edgecolors='c', s=70)  # Plot points
    for xy in zip(fpgrowth_ticks, support):
        plt.annotate('(%s, %s)' % xy, xy=xy, textcoords='data')  # Annotate coordinates
    
    plt.title("Runtimes for FP Growth Vs. Minimum Support")  # Add title
    plt.xlabel("FP Growth Runtimes (In Seconds)")  # Add x-axis label
    plt.xticks(fpgrowth_ticks, fpgrowth_ticks)  # Mark x-axis ticks
    plt.ylabel("Minimum Support Values")  # Add y-axis label
    
    plt.grid()  # Show grid and graph
    plt.show()


"""
Perform the algorithms and store the times taken to run through each one as per 
"""
def plot_min_sup(transactions):
    # These lists are to store the itemsets when minimum support is 10%
    apriori_10 = []
    fpgrowth_10 = []
    
    # For all the minimum support values, lets get the runtimes
    for sup in minSup:
        # Calculate Apriori
        print "\n\nCalculating Apriori when minimum support is " + str(int((sup) * 100)) + "%"
        start_time = time.time()  # Start timer
        data_apriori = runApriori(transactions, sup)  # Frequent items with Apriori
        
        end_time = (time.time() - start_time)  # End timer
        apriori_times.append(end_time)  # Store time
        print " -> Completed in approximately " + str(int(np.floor(end_time))) + " second(s)."
        
        
        # Calculate FP Growth
        print "Calculating FP Growth when minimum support is " + str(int((sup) * 100)) + "%"
        start_time = time.time()  # Start timer
        itemsets = find_frequent_itemsets(transactions, sup * len(transactions), True)
        
        # Frequent items with FP Growth
        data_fpgrowth = []
        for itemset, support in itemsets:
            data_fpgrowth.append((itemset, float(support)/len(transactions)))
        
        
        end_time = (time.time() - start_time)  # End timer
        fpgrowth_times.append(end_time)  # Store time
        print " -> Completed in approximately " + str(int(np.floor(end_time))) + " second(s)."
        
        # Store the itemsets for when minimum support is 10% for the next question
        if sup == 0.10:
            apriori_10 = data_apriori
            fpgrowth_10 = data_fpgrowth
        
        # End of loop
        
    # Send to be plotted
    print "\n\nCreating runtime plots for minimum support values"
    create_plots(apriori_times, fpgrowth_times, minSup)
    
    # Save total time as an approximation
    total_seconds = np.sum(apriori_times)
    total_seconds = total_seconds + np.sum(fpgrowth_times)
    total_seconds = int(np.floor(total_seconds))
    print "\n\nTotal time taken is approximately " + str(total_seconds) + " seconds."
    
    return apriori_10, fpgrowth_10  # Return these itemsets for the next questions


# Calculating Apriori and FP Growth, store the itemsets for when minSup = 10%
data_apriori, data_fpgrowth = plot_min_sup(transactions)

# Store runtimes in dataframe
df_runtimes = pd.DataFrame(list(zip(minSup, apriori_times, fpgrowth_times)),
              columns=['Minimum Support Value','Apriori Runtimes', 'FP Growth Runtimes'])

# Show runtimes as a table as well
print "\n\n\t\t\tTable of Runtimes\n"
with pd.option_context('display.max_rows', None, 'display.max_columns', 3):
    print(df_runtimes)

# Plot Histogram for len(frequent_itemsets) where MinSup = 10%
print "\n\nPlotting Histograms"
frequent_itemset_lengths_apriori = []
frequent_itemset_lengths_fpgrowth = []

for instance in data_apriori:
    frequent_itemset_lengths_apriori.append(len(instance[0]))

for instance in data_fpgrowth:
    frequent_itemset_lengths_fpgrowth.append(len(instance[0]))
    
print "\n\nApriori Histogram for the lengths of frequent itemsets when minimum support = 10%\n"
histogram(frequent_itemset_lengths_apriori, "Histogram of lengths of frequent itemsets [Apriori]", "Itemsets", "Blue")

print "\n\nFP Growth Histogram for the lengths of frequent itemsets when minimum support = 10%\n"
histogram(frequent_itemset_lengths_fpgrowth, "Histogram of lengths of frequent itemsets [FP Growth]", "Itemsets", "Green")


"""
Histogram data is equal, which means that both algorithms arrived at the same
result. So, for questions 4 & 5, I'm going to focus on the itemset data we've gotten
by running FP Growth, simply because that is the algorithm I'd use if I had to choose
between the two.
"""
freq_itemset = data_fpgrowth  # Assign the FP Growth data to a new variable for the next question

# For MinSup = 10%, pick 5 itemsets where len(itemset) > 3 && income <=50k. Find confidence of rule
print "\n\n\nWorking on itemsets that contain \" <=50K\":"
filtered_itemset_more = [v for v in freq_itemset if (' <=50K' in v[0] and len(v[0]) >= 3)]
print "Itemset filtered."

print "Picking 5 random itemsets"
itemset_more = [random.choice(filtered_itemset_more) for i in range(5)]
print "\n\nItemsets:\n"
for itemset in itemset_more:
    print itemset
    


# For MinSup = 10%, pick 5 itemsets where len(itemset) > 3 && income >50k. Find confidence of rule
print "\n\n\nWorking on itemsets that contain \" >50K\":"
filtered_itemset_less = [v for v in freq_itemset if (' >50K' in v[0] and len(v[0]) >= 3)]
print "Itemset filtered."

print "Picking 5 random itemsets"
itemset_less = [random.choice(filtered_itemset_less) for i in range(5)]
print "\n\nItemsets:\n"
for itemset in itemset_less:
    print itemset

print "\n\n\nComplete.\n"
# --------------------------------------------------------------
#            Tushar Iyer | CSCI 720 | Spring 2018
# --------------------------------------------------------------