import csv
from apriori import *
from fp_growth import *

#transaction data
transaction_file = 'example.csv'
minSupport = 0.15
transactions = []
with open(transaction_file, 'rU') as database:
    for row in csv.reader(database):
        transactions.append(row)

#run apriori
print("apriori")
frequent_items1= runApriori(transactions, minSupport)
print frequent_items1

#run FPgrowth
print("\n\nFPgrowth")
itemsets = find_frequent_itemsets(transactions, minSupport*len(transactions), True)
print(itemsets)
frequent_items2 = []
for itemset, support in itemsets:
    frequent_items2.append((itemset, float(support)/len(transactions)))
print frequent_items2
