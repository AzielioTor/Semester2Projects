"""
Big Data Analytics - Homework 4
Aziel Shaw - 309000893
02/14/2018
"""


# Imports
import pandas as pd
import numpy as np
from apriori import runApriori
from fp_growth import find_frequent_itemsets
import matplotlib.pyplot as plt
import datetime
import random

def create_histogram(data, title="Histogram Title", xLabel="xLabel", yLabel="yLabel"):
    # Create Graph
    counts, bins, bars = plt.hist(data)
    plt.title(title, fontsize=20)
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.colormaps()
    # Show Graph
    plt.show();
    # Get data from histogram
    print(counts)
    print(bins)
    # Store data
    # Return the data from this histogram
    return bins, counts

total_program_start_time = datetime.datetime.now()
# Read in Dataset
X_all = pd.read_csv("adult.data", header=None)
# Remove unneeded columns
X = pd.read_csv("adult.data", header=None, usecols=[0, 1, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13 ,14])
# Add columns headers
column_headers = ['age', 'workclass', 'eduaction', 'marital-status', 'occupation', 
           'relationship', 'race', 'sex', 'capital-gain', 'capital-lost', 
           'hours-per-week', 'native-country', 'income']
X.columns = column_headers

# QUESTION 1
# Describe your preprocessing.  
# What are the total number of items in the transaction data? 
# Convert continuous features age, capital gain/loss, hours-per-week to 
#     categories (using histograms or other method)
age = X['age'].tolist()
capital_gain = X['capital-gain'].tolist()
capital_loss = X['capital-lost'].tolist()
hours_per_week = X['hours-per-week'].tolist()
age_bins, age_counts = create_histogram(age, "Age", "Age", "Amount of people this age")
gain_bins, gain_counts = create_histogram(capital_gain, "Capital Gain", "Measures of Capital Gain", "Amount of people with this Capital Gain")
loss_bins, loss_counts = create_histogram(capital_loss, "Capital Loss", "Measures of Capital Loss", "Amount of people with this Capital Loss")
hours_bins, hours_counts = create_histogram(hours_per_week, "Hours per week", "Hours people work each week", "Amount of people who work this many hours")

categorical_age = []
categorical_gain = []
categorical_loss = []
categorical_hours = []

for a in age:
    toAppend = 0
    if 17     <= a < 24.2: toAppend = 1
    elif 24.3 <= a < 31.5: toAppend = 2
    elif 31.6 <= a < 38.8: toAppend = 3
    elif 38.9 <= a < 46.1: toAppend = 4
    elif 46.2 <= a < 53.4: toAppend = 5
    elif 53.5 <= a < 60.7: toAppend = 6
    elif 60.8 <= a < 68:   toAppend = 7
    elif 68.1 <= a < 75.3: toAppend = 8
    elif 75.4 <= a < 82.6: toAppend = 9
    elif 82.7 <= a < 89.9: toAppend = 10
    elif 90   <= a:        toAppend = 11
    else: toAppend = -1
    categorical_age.append(str(toAppend))
    
for g in capital_gain:
    toAppend = 0
    if 0         <= g < 9999.8:  toAppend = 1
    elif 9999.9  <= g < 19999.7: toAppend = 2
    elif 19999.8 <= g < 29999.6: toAppend = 3
    elif 29999.7 <= g < 39999.5: toAppend = 4
    elif 39999.6 <= g < 49999.4: toAppend = 5
    elif 49999.5 <= g < 59999.3: toAppend = 6
    elif 59999.4 <= g < 69999.2: toAppend = 7
    elif 69999.3 <= g < 79999.1: toAppend = 8
    elif 79999.2 <= g < 89999.0: toAppend = 9
    elif 89999.1 <= g < 99998.9: toAppend = 10
    elif 99999.0 <= g:           toAppend = 11
    else: toAppend = -1
    categorical_gain.append(str(toAppend))
    
for l in capital_loss:
    toAppend = 0
    if 0        <= l <  435.5: toAppend = 1
    elif 435.6  <= l <  871.1: toAppend = 2
    elif 871.2  <= l < 1306.7: toAppend = 3
    elif 1306.8 <= l < 1742.3: toAppend = 4
    elif 1742.4 <= l < 2177.9: toAppend = 5
    elif 2178.0 <= l < 2613.5: toAppend = 6
    elif 2613.6 <= l < 3049.1: toAppend = 7
    elif 3049.2 <= l < 3484.7: toAppend = 8
    elif 3484.8 <= l < 3920.3: toAppend = 9
    elif 3920.4 <= l < 4355.9: toAppend = 10
    elif 4356.0 <= l:          toAppend = 11
    else: toAppend = -1
    categorical_loss.append(str(toAppend))
    
for h in hours_per_week:
    toAppend = 0
    if 1      <= h < 10.7: toAppend = 1
    elif 10.8 <= h < 20.5: toAppend = 2
    elif 20.6 <= h < 30.3: toAppend = 3
    elif 30.4 <= h < 40.1: toAppend = 4
    elif 40.2 <= h < 49.9: toAppend = 5
    elif 50.0 <= h < 59.7: toAppend = 6
    elif 59.8 <= h < 69.5: toAppend = 7
    elif 69.6 <= h < 79.3: toAppend = 8
    elif 79.4 <= h < 89.1: toAppend = 9
    elif 89.2 <= h < 98.9: toAppend = 10
    elif 99.0 <= h:        toAppend = 11
    else: toAppend = -1
    categorical_hours.append(str(toAppend))

X['age'] = categorical_age
X['capital-gain'] = categorical_gain
X['capital-lost'] = categorical_loss
X['hours-per-week'] = categorical_hours

# QUESTION 2
# Plot run times of Apriori and FP Growth versus Minimum Support. 
# Use minSup of 2%, 5% 10%, 15% 20% , 50%. 
# Explain the differences in the run times of the two algorithms.  

akfass = []

for rows in X.itertuples():
    akfass.append(list(rows))  

minimum_support = [0.02, 0.05, 0.10, 0.15, 0.20, 0.50]
minimum_support = [0.50]

#for ms in minimum_support:
#    print("\nRunning Apriori with minimum Support :  " + str(ms))
#    start_time_apriori = datetime.datetime.now()
#    frequent_items_apriori = runApriori(akfass, ms)
#    # print frequent_items_apriori
#    finish_time_apriori = datetime.datetime.now() 
#    total_time_apriori = finish_time_apriori - start_time_apriori
#    print(total_time_apriori)
#    print(frequent_items_apriori)
#    
#    print("\nRunning FPGrowth with minimum Support :  " + str(ms))
#    start_time_fpgrowth = datetime.datetime.now()
#    frequent_items_fpgrowth = find_frequent_itemsets(akfass, ms*len(akfass), True)
#    finish_time_fpgrowth = datetime.datetime.now()
#    total_time_fpgrowth = finish_time_fpgrowth - start_time_fpgrowth
#    print(total_time_fpgrowth)
    

    
    
# QUESTION 3
# Plot a histogram of the length of frequent itemsets obtained for 
#     minSup=10%. 
    
minSup = 0.10
# Apriori
#itemsets_apriori = runApriori(X, minSup)
#create_histogram(itemsets_apriori, "Apriori")
# FPGrowth
itemsets_fpgrowth = find_frequent_itemsets(akfass, minSup * len(akfass), True)
items_fpgrowth = []
for datasets, supportdata in itemsets_fpgrowth:
    items_fpgrowth.append((datasets, float(supportdata)/len(akfass)))
print("\n\nDONE Question 3\n")

item_lengths = []
for itemset in items_fpgrowth:
    item_lengths.append(len(itemset[0]))

print(items_fpgrowth)
len_bins, len_counts = create_histogram(item_lengths, "Lengths of items", "Amounts of list in items", "Amount of item lists with amount")

#print("\n")
## QUESTION 4
## For minSup=10%, choose 5 frequent itemsets that contains "<=50K"
## (choose itemsets with more than 3 items).  
## Consider the rule  X -> "<50 K", where X are the  other items in your 
##     frequent itemset.  
## Find the confidence of your rule for each of the frequent itemsets you chose. 
#Q_4_big_list = []
#Q_4_small_list = []
#for element in items_fpgrowth:
#    if len(element[0]) > 2 and ' <=50K' in element[0]:
#        Q_4_big_list.append(element)
#for index in range(0, 6):
#    Q_4_small_list.append(Q_4_big_list[random.randint(0, len(Q_4_big_list))])
#for listy in Q_4_small_list:
#    print(listy)
#print("\n")
#
#
## QUESTION 5
## Repeat (4) for 5 frequent itemsets that contains ">50K".
#
#Q_5_big_list = []
#Q_5_small_list = []
#for element in items_fpgrowth:
#    if len(element[0]) > 2 and ' >50K' in element[0]:
#        Q_5_big_list.append(element)
#for index in range(0, 6):
#    Q_5_small_list.append(Q_5_big_list[random.randint(0, len(Q_5_big_list))])
#for listy in Q_5_small_list:
#    print(listy)
#
#
#
#
#print("Total Program Runtime")
#print(datetime.datetime.now() - total_program_start_time)

