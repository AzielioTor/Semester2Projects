"""
Big Data Analytics - Assignment 5
Aziel Shaw - 309000893
25/02/2018
"""


from __future__ import division
#  IMPORTS
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.datasets import fetch_20newsgroups
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn import metrics
import matplotlib as plt
import numpy as np

# Cleaning Function
def remove_nonexistant_words(data):
    
    return data

# Perform Analysis
def perform_analysis(data_train, data_test, dataname):
    # Initialize preprocessing Routing
    sparsity_count = []
    cv = CountVectorizer()
	
    #CLEAN DATA BEFORE FITTING ANYTHING ELSE HERE
    data_train_clean = remove_nonexistant_words(data_train.data)
    data_test_clean = remove_nonexistant_words(data_test.data)
	
    # Uncomment above once cleaning function is done. If this doesn't work, then do the cleaning function first.
	
    print("\n\nPerformaing Analysis on " + dataname + "\n")
	
    cv.fit_transform(data_train.data)  # REMOVE THE .DATA AFTER CLEANING IS IMLEMENTED

    print("\n\nLength of Unique Vocabulary: ")
    vocab = cv.vocabulary_
    print(str(len(vocab)) + "\n")
	
    # Sparsity
    vocab_occurences = (vocab).values()
    # This is the function I'm using
    for word_count in vocab_occurences:
        sparsity_count.append(word_count / len(vocab))
    
    final_sparsity = np.mean(sparsity_count)
    print("Average Sparsity for training data: " + "{0:.5f}".format(final_sparsity) )
    
    data_pipeline = Pipeline([('vect', CountVectorizer()),
                         ('tfidf', TfidfTransformer()),
                         ('clf', MultinomialNB()),])
    
    data_pipeline.fit(data_train.data, data_train.target)
    
    predicted_results = data_pipeline.predict(data_test.data)
    
    means = np.mean(predicted_results == data_test.target)
    print("Accuracy of " + dataname + "  : " + str(means) + "\n")
    print("Classification Report for " + dataname)
    print(metrics.classification_report(data_test.target, predicted_results, target_names=data_test.target_names))
    print("\nConfusion Matrix")
    print(metrics.confusion_matrix(data_test.target, predicted_results))
    
    print("\nComplete.")


def prep_data():
    # For 4 categories, use the smaller set
    class_categories = ['alt.atheism', 'talk.religion.misc', 'comp.graphics', 'sci.space']
    
    # Small set variables
    train_small = fetch_20newsgroups(subset='train', categories=class_categories)
    test_small = fetch_20newsgroups(subset='test', categories=class_categories)
    
    # Full set variables
    train_full = fetch_20newsgroups(subset='train')
    test_full = fetch_20newsgroups(subset='test')
    
    # Call main function
    perform_analysis(train_small,test_small,"4 Categories")
    perform_analysis(train_full ,test_full ,"all Categories")

prep_data() # Run progam here


#*Program Tracethrough*
#- Start DONE
#- Define lists [four/twenty categories] DONE
#- Call main routine with [four] and then with [twenty] DONE
#
#*Main*
#- Extract data specific to categories DONE
#- Clean Data (both train and test)    LATER - Line 36
#- Create countVec                     DONE
#- Transform with tfidfTransformer     DONE
#- print len(vocab)                    
#- Create Pipeline:                    DONE??
#- CountVec                            ??
#- TfidfTransform                      ??
#- MultinomialNB                       ??
#- Fit classifier with train.data
#- predict with classifier using test data. Save into variable
#- Print accuracy
#- Plot Conf.Matrix
#
#*Clean*
#- Create match cases for the shit you want to strip (common words, punctuation, etc)
#- For each line in data, remove any matches
#- Return modified data
#
#*Plot*
#- create matrix with `sklearn.metrics.confusion_matrix`
#- plt.matshow
#- Label axes
#- plt.show()