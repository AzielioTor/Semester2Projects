"""
Big Data Analytics - Assignment 5
Aziel Shaw - 309000893
25/02/2018
"""


from __future__ import division
#  IMPORTS
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.datasets import fetch_20newsgroups
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
import re
from sklearn import metrics
import numpy as np

# Cleaning Function
def remove_nonexistant_words(data):
    words_to_remove = r'\band\b|\bthe\b|[>]+|[<]+|[.]+|\breally\b|\bof\b|@|\b.com\b|\b.edu\b|\b.gov\b'
    for article, word in enumerate(data):
		word = re.sub(words_to_remove, "", word)
		data[article] = word
    return data

# Perform Analysis
def perform_analysis(data_train, data_test, dataname, alpha):
    # Initialize preprocessing Routing
    sparsity_count = []
    cv = CountVectorizer()
	
    data_train_clean = remove_nonexistant_words(data_train.data)
    data_test_clean = remove_nonexistant_words(data_test.data)
	
	
    print("\n\nPerformaing Analysis on " + dataname + " with alpha of " + str(alpha) + "\n")
	
    cv.fit_transform(data_train_clean)

    print("\n\nLength of Unique Vocabulary: ")
    vocab = cv.vocabulary_
    print(str(len(vocab)) + "\n")
	
    # Sparsity
    vocab_occurences = (vocab).values()
    for word_count in vocab_occurences:
        sparsity_count.append(word_count / len(vocab))
    
    final_sparsity = np.mean(sparsity_count)
    print("Average Sparsity for training data: " + "{0:.5f}".format(final_sparsity) )
    
    data_pipeline = Pipeline([('vect', CountVectorizer()),
                         ('tfidf', TfidfTransformer()),
                         ('clf', MultinomialNB(alpha=alpha)),])
    
    data_pipeline.fit(data_train_clean, data_train.target)
    
    predicted_results = data_pipeline.predict(data_test_clean)
    
    means = np.mean(predicted_results == data_test.target)
    print("Accuracy of " + dataname + "  : " + str(means) + "\n")
    print("Classification Report for " + dataname)
    print(metrics.classification_report(data_test.target, predicted_results, target_names=data_test.target_names))
    print("\nConfusion Matrix")
    print(metrics.confusion_matrix(data_test.target, predicted_results))
    
    print("\nComplete.")


def prep_data(alpha):
    # For 4 categories, use the smaller set
    class_categories = ['alt.atheism', 'talk.religion.misc', 'comp.graphics', 'sci.space']
    
    # Small set variables
    train_small = fetch_20newsgroups(subset='train', categories=class_categories)
    test_small = fetch_20newsgroups(subset='test', categories=class_categories)
    
    # Full set variables
    train_full = fetch_20newsgroups(subset='train')
    test_full = fetch_20newsgroups(subset='test')
    
    # Call main function
    perform_analysis(train_small,test_small,"4 Categories", alpha)
    perform_analysis(train_full ,test_full ,"all Categories", alpha)

alphas = [0.05, 0.20, 0.50, 1.00, 0.4, 0.001, 0.60, 0.30]
for alpha in alphas:
    prep_data(alpha) # Run progam here
